#!/bin/bash

# Install Composer
EXPECTED_SIGNATURE=$(curl https://composer.github.io/installer.sig)
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
ACTUAL_SIGNATURE=$(php -r "echo hash_file('SHA384', 'composer-setup.php');")

if [ "$EXPECTED_SIGNATURE" != "$ACTUAL_SIGNATURE" ]
then
    >&2 echo 'ERROR: Invalid installer signature'
    rm composer-setup.php
    exit 1
fi

php composer-setup.php --quiet
rm composer-setup.php

# Prereqs for Cake PHP
apt-get update
apt-get install -y libicu-dev
docker-php-ext-install intl
docker-php-ext-install pdo_mysql

# Other prereqs
apt-get install -y git zlib1g-dev
docker-php-ext-install zip
apt-get install -y lftp

# Install everything
php composer.phar install
