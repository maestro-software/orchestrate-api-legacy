<?php
namespace App\Test\TestCase\Controller;

use App\Controller\AssetLoanController;
use Cake\Controller\ComponentRegistry;
use App\Controller\Component\AssetLoanServiceComponent;
use \DateTime;

class AssetLoanControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->AssetLoanService = new AssetLoanServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->AssetLoanService);
        parent::tearDown();
    }

    public function testView() {
       $loan = $this->get('/asset_loan/1')->assetLoan;
       $this->assertResponseOk();
       $this->assertEquals(1, $loan->assetId);
       $this->assertEquals(1, $loan->memberId);
    }

    public function testViewNotFound() {
        $this->get('/asset_loan/0');
        $this->assertResponseCode(404);
    }

    public function testUpdateDateBorrowed() {
        $updateMap = ['dateBorrowed' => new DateTime()];

        $oldDateBorrowed = $this->AssetLoanService->findById(1)->dateBorrowed;
        $loan = $this->put('/asset_loan/1', $updateMap)->assetLoan;
        $this->assertResponseOk();
        $this->assertNotEquals($oldDateBorrowed, new DateTime($loan->dateBorrowed));
    }

    public function testUpdateDateReturned() {
        $updateMap = ['dateReturned' => new DateTime()];

        $oldDateReturned = $this->AssetLoanService->findById(1)->dateReturned;

        $loan = $this->put('/asset_loan/1', $updateMap)->assetLoan;
        $this->assertResponseOk();
        $this->assertNotEquals($oldDateReturned, new DateTime($loan->dateReturned));
    }

    public function testUpdateCantUpdateAssetOrMember() {
        $updateMap = [
            'assetId' => 3,
            'memberId' => 3
        ];

        $responseData = $this->put('/asset_loan/1', $updateMap);
        $this->assertResponseOk();
        $this->assertNotEquals(3, $responseData->assetLoan->assetId);
        $this->assertNotEquals(3, $responseData->assetLoan->memberId);
    }

    public function testDelete() {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->delete('/asset_loan/1');
        $this->assertResponseOk();
        $this->AssetLoanService->findById(1);
    }

    public function testGetLoanHistoryByAssetId() {
        $responseData = $this->get('/asset/1/loan');
        $this->assertResponseOk();
        $this->assertCount(2, $responseData->assetLoan);
    }

    public function testGetCurrentLoan() {
        $responseData = $this->get('/asset/2/loan/current');
        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->assetLoan->member->id);
    }

    public function testGetCurrentLoanNotOnLoan() {
        $this->get('/asset/1/loan/current');
        $this->assertResponseCode(404);
    }

    public function testLoanAssetToMember() {
        $responseData = $this->post('asset/1/loan/to/member/1');
        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->assetLoan->member->id);
    }

    public function testLoanUnknownAsset() {
        $responseData = $this->post('asset/0/loan/to/member/1');
        $this->assertResponseCode(404);
    }

    public function testLoanAssetToUnknownMember() {
        $responseData = $this->post('asset/1/loan/to/member/0');
        $this->assertResponseCode(404);
    }

    public function testLoanAssetToMemberAlreadyOnLoanToSameMember() {
        $loanCountBefore = $this->AssetLoanService->getHistoryByAssetId(2)->count();
        $responseData = $this->post('asset/2/loan/to/member/1');
        $loanCountAfter = $this->AssetLoanService->getHistoryByAssetId(2)->count();

        $this->assertResponseOk();
        $this->assertEquals($loanCountBefore, $loanCountAfter);
    }

    public function testLoanAssetToMemberAlreadyOnLoanToDifferentMember() {
        $responseData = $this->post('asset/2/loan/to/member/2');
        $this->assertResponseCode(400);
    }

    public function testReturnAssetLoan() {
        $responseData = $this->post('asset/2/loan/return');
        $this->assertResponseOk();
        $this->assertNotNull($responseData->assetLoan->dateReturned);
    }

    public function testReturnAssetNotOnLoan() {
        $responseData = $this->post('asset/1/loan/return');
        $this->assertResponseOk();
        $this->assertNotNull($responseData->assetLoan->dateReturned);
    }

    public function testGetLoanHistory() {
        $responseData = $this->get('asset/1/loan');
        $this->assertResponseOk();
        $this->assertCount(2, $responseData->assetLoan);
        $this->assertNotNull($responseData->assetLoan[0]->member);
    }
}
