<?php

namespace App\Test\TestCase\Controller;

use Cake\Controller\ComponentRegistry;
use App\Test\TestCase\DomainObjectFactory;
use Cake\TestSuite\IntegrationTestTrait;
use Cake\TestSuite\TestCase;

abstract class RestTestCase extends TestCase
{
    use IntegrationTestTrait {
        get as public traitGet;
        post as public traitPost;
        put as public traitPut;
    }

    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.MemberInstrument',
        'app.Asset',
        'app.AssetLoan',
        'app.Ensemble',
        'app.EnsembleMembership',
        'app.Concert',
        'app.Performance',
        'app.Score',
        'app.PerformanceScore',
        'app.PerformanceMember',
        'app.Role',
        'app.UserRole'
    ];

    public function setUp()
    {
        parent::setUp();

        $this->configRequest([
            'headers' => ['Accept' => 'application/json']
        ]);

        /*
         * Force an authenticated user (we will do seperate controller
         * authentication/authorisation tests)
         */
        $this->session(['Auth' => ['User' => DomainObjectFactory::createUserMap()]]);
        $this->enableCsrfToken();

        $this->registry = new ComponentRegistry();
    }

    public function tearDown()
    {
        unset($this->registry);
    }

    public function get($path)
    {
        $this->traitGet($path);
        return $this->getResponseBody();
    }

    public function post($path, $body = [])
    {
        $this->traitPost($path, $body);
        return $this->getResponseBody();
    }

    public function put($path, $body = [])
    {
        $this->traitPut($path, $body);
        return $this->getResponseBody();
    }

    private function getResponseBody()
    {
        return json_decode($this->_response->getBody());
    }
}
