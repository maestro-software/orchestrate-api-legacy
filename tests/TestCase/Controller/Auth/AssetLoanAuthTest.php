<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Test\TestCase\DomainObjectFactory;

class AssetLoanAuthTest extends AuthTestCase {
    public function testGetById() {
        $endpoint = '/asset_loan/1';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testPut() {
        $endpoint = '/asset_loan/1';
        $this->assertStandardPutAccessForEndpoint($endpoint, []);
    }

    public function testDelete() {
        $endpoint = '/asset_loan/1';
        $mockAssetLoan = DomainObjectFactory::createAssetLoanMap();
        $recreateAssetLoan = $this->generateNewObjectFunctionWithId('AssetLoan', $mockAssetLoan, 1);
        $this->assertStandardDeleteAccessForEndpoint($endpoint, $recreateAssetLoan);
    }

    public function testGetLoanHistory() {
        $endpoint = '/asset/1/loan';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testLoanToMember() {
        $endpoint = '/asset/1/loan/to/member/1';
        $this->assertStandardPostAccessForEndpoint($endpoint, []);
    }

    public function testReturnLoan() {
        $endpoint = '/asset/1/loan/return';
        $this->assertStandardPostAccessForEndpoint($endpoint, []);
    }

    public function testGetCurrectLoan() {
        $endpoint = '/asset/2/loan/current';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }
}