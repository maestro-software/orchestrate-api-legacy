<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Test\TestCase\DomainObjectFactory;

class PerformanceControllerAuthTest extends AuthTestCase {
    public function testPerformanceEndpoint() {
        $endpoint = '/performance';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testPerformanceIdEndpoint() {
        $endpoint = '/performance/1';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testConcertPerformanceEndpoint() {
        $endpoint = '/concert/1/performance';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testPost() {
        $performance = DomainObjectFactory::createPerformanceMap();
        $performance['concertId'] = 1;
        $performance['ensembleId'] = 3;

        $this->assertStandardPostAccessForEndpoint('/performance', $performance);
    }

    public function testPut() {
        $performance = [
            'ensembleId' => 3
        ];
        $this->assertStandardPutAccessForEndpoint('/performance/1', $performance);
    }

    public function testDelete() {
        $endpoint = '/performance/1';
        $mockPerf = DomainObjectFactory::createPerformanceMap();
        $recreateCallback = $this->generateNewObjectFunctionWithId('Performance', $mockPerf, 1);
        $this->assertStandardDeleteAccessForEndpoint($endpoint, $recreateCallback);
    }
}