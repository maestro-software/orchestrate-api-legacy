<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Controller\UsersController;
use App\Test\TestCase\DomainObjectFactory;

class UsersControllerAuthTest extends AuthTestCase {
    private $authorisedUsers = ['admin'];
    private $deniedUsers = ['editor', 'viewer', 'unknown'];

    public function testIndex() {
        $endpoint = '/users';
        $this->assertGetAccessForEndpoint($endpoint, $this->authorisedUsers, $this->deniedUsers);
    }

    public function testView() {
        $endpoint = '/users/1';
        $this->assertGetAccessForEndpoint($endpoint, $this->authorisedUsers, $this->deniedUsers);
    }

    public function testAdd() {
        $newUser = DomainObjectFactory::createUserMap();
        unset($newUser['id']);
        $newUser['username'] = 'newuser';

        $endpoint = '/users';
        $this->assertPostAccessForEndpoint($endpoint, $newUser, $this->authorisedUsers, $this->deniedUsers);
    }

    public function testEdit() {
        $modifiedUser = ['username' => 'modifieduser'];

        $endpoint = '/users/1';
        $this->assertPutAccessForEndpoint('/users/1', $modifiedUser, $this->authorisedUsers, $this->deniedUsers);
    }

    public function testDelete() {
        $mockUser = DomainObjectFactory::createUserMap();
        $recreateCallback = $this->generateNewObjectFunctionWithId('Users', $mockUser, 1);
        $endpoint = '/users/1';
        $this->assertDeleteAccessForEndpoint($endpoint, $recreateCallback, $this->authorisedUsers, $this->deniedUsers);
    }
}
