<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Controller\AssetController;
use App\Test\TestCase\DomainObjectFactory;

class AssetControllerAuthTest extends AuthTestCase {
    public function testIndex() {
        $endpoint = '/asset';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testView() {
        $endpoint = '/asset/1';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testAdd() {
        $endpoint = '/asset';

        $newAsset = DomainObjectFactory::createAssetMap();
        unset($newAsset['id']);
        $newAsset['description'] = 'Bass Clarinet';

        $this->assertStandardPostAccessForEndpoint($endpoint, $newAsset);
    }

    public function testEditAdmin() {
        $endpoint = '/asset/1';
        $modifiedAsset = ['description' => 'Bass Clarinet'];

        $this->assertStandardPutAccessForEndpoint($endpoint, $modifiedAsset);
    }

    public function testDeleteAdmin() {
        $endpoint = 'asset/1';
        $recreateAsset = $this->generateNewObjectFunctionWithId('Asset', DomainObjectFactory::createAssetMap(), 1);
        $this->assertStandardDeleteAccessForEndpoint($endpoint, $recreateAsset);
    }
}
