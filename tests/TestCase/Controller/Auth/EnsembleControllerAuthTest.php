<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Controller\EnsembleController;
use App\Test\TestCase\DomainObjectFactory;

class EnsembleControllerAuthTestTest extends AuthTestCase {
    public function testIndexAdmin() {
        $endpoint = '/ensemble';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testView() {
        $endpoint = '/ensemble/1';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testAdd() {
        $endpoint = '/ensemble';

        $newEnsemble = DomainObjectFactory::createEnsembleMap();
        unset($newEnsemble['id']);
        $newEnsemble['name'] = 'Sax Ensemble';

        $this->assertPostOkAsRole($endpoint, $newEnsemble, 'admin');
        $newEnsemble['name'] = 'Sax Ensemble2'; // Has to be unique -.-
        $this->assertPostOkAsRole($endpoint, $newEnsemble, 'editor');
        $this->assertPostUnauthorizedAsRole($endpoint, $newEnsemble, 'viewer');
        $this->assertPostUnauthorizedAsRole($endpoint, $newEnsemble, 'unknown');
    }

    public function testEdit() {
        $endpoint = '/ensemble/1';
        $newEnsemble = ['name' => 'Sax Ensemble'];

        $this->assertStandardPutAccessForEndpoint($endpoint, $newEnsemble);
    }

    public function testDelete() {
        $endpoint = '/ensemble/1';
        $mockEnsemble = DomainObjectFactory::createEnsembleMap();
        $recreateAsset = $this->generateNewObjectFunctionWithId('Ensemble', $mockEnsemble, 1);
        $this->assertStandardDeleteAccessForEndpoint($endpoint, $recreateAsset);
    }
}
