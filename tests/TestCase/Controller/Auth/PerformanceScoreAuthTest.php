<?php
namespace App\Test\TestCase\Controller\Auth;

class PerformanceScoreAuthTest extends AuthTestCase {
    public function testGetPerformanceScores() {
        $endpoint = '/performance/1/score';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testPostPerformanceScore() {
        $endpoint = '/performance/1/score/1';
        $this->assertStandardPostAccessForEndpoint($endpoint, []);
    }

    public function testDeletePerformanceScore() {
        $endpoint = 'performance/1/score/1';
        $this->assertStandardDeleteAccessForEndpoint($endpoint);
    }
}