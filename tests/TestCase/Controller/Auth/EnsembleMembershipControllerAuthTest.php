<?php
namespace App\Test\TestCase\Controller\Auth;

use App\Controller\EnsembleController;
use Cake\TestSuite\IntegrationTestCase;
use App\Test\TestCase\DomainObjectFactory;

class EnsembleMembershipControllerAuthTest extends AuthTestCase {
    public function testEnsembleMembership() {
        $endpoint = '/ensemble/1/ensemble_membership';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testCurrentEnsembleMembership() {
        $endpoint = '/ensemble/1/ensemble_membership/current';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testPastEnsembleMembership() {
        $endpoint = '/ensemble/1/ensemble_membership/past';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testJoin() {
        $endpoint = '/ensemble/1/ensemble_membership/3/join';

        $this->assertStandardPostAccessForEndpoint($endpoint);
    }

    public function testLeave() {
        $endpoint = '/ensemble/1/ensemble_membership/1/leave';

        $this->assertStandardPostAccessForEndpoint($endpoint);
    }

    public function testGetMembershipById() {
        $endpoint = '/ensemble_membership/1';
        $this->assertStandardGetAccessForEndpoint($endpoint);
    }

    public function testUpdateMembership() {
        $endpoint = '/ensemble_membership/1';
        $this->assertStandardPutAccessForEndpoint($endpoint);
    }

    public function testDelete() {
        $endpoint = '/ensemble_membership/1';
        $newMembership = DomainObjectFactory::createEnsembleMembershipMap();
        $createMembershipCallback = $this->generateNewObjectFunctionWithId('EnsembleMembership', $newMembership, 1);
        $this->assertStandardDeleteAccessForEndpoint($endpoint, $createMembershipCallback);
    }
}
