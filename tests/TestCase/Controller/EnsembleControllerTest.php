<?php
namespace App\Test\TestCase\Controller;

use App\Controller\EnsembleController;
use App\Test\TestCase\Controller\RestTestCase;
use App\Controller\Component\EnsembleServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\Datasource\Exception\RecordNotFoundException;
use App\Test\TestCase\DomainObjectFactory;

class EnsembleControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->EnsembleService = new EnsembleServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->EnsembleService);

        parent::tearDown();
    }

    public function testIndex() {
        $responseData = $this->get('/ensemble');

        $this->assertResponseOk();
        $this->assertResponseContains('ensemble');
        $this->assertCount(3, $responseData->ensemble);
    }

    public function testViewExists() {
        $responseData = $this->get('/ensemble/1');

        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->ensemble->id);
    }

    public function testViewNotExists() {
        $this->get('/ensemble/0');

        $this->assertResponseCode(404);
    }

    public function testAdd() {
        $newEnsemble = DomainObjectFactory::createEnsembleMap();
        unset($newEnsemble['id']);
        $newEnsemble['name'] = 'Sax Ensemble';

        $responseData = $this->post('/ensemble', $newEnsemble);

        $this->assertResponseOk();
        $this->assertEquals('Sax Ensemble', $responseData->ensemble->name);
        $this->assertGreaterThan(0, $responseData->ensemble->id);

        // Make sure it was actually created.
        $this->EnsembleService->findById($responseData->ensemble->id);
    }

    public function testAddInvalid() {
        $newEnsemble = DomainObjectFactory::createEnsembleMap();
        $newEnsemble['name'] = '';

        $countBefore = $this->EnsembleService->count();
        $this->post('/ensemble', $newEnsemble);
        $countAfter = $this->EnsembleService->count();

        $this->assertResponseCode(400);
        $this->assertEquals($countBefore, $countAfter);
    }

    public function testEdit() {
        $modifiedEnsemble = $this->EnsembleService->findById(1);
        $modifiedEnsemble->name = 'Leeming Concert';

        $responseData = $this->put('/ensemble/1', $modifiedEnsemble->toArray());

        $this->assertResponseOk();
        $this->assertEquals('Leeming Concert', $responseData->ensemble->name);
        $this->assertEquals(1, $responseData->ensemble->id);

        // Make sure it was actually updated.
        $dbEnsemble = $this->EnsembleService->findById($responseData->ensemble->id);
        $this->assertEquals('Leeming Concert', $dbEnsemble->name);
    }

    public function testEditInvalid() {
        $modifiedEnsemble = $this->EnsembleService->findById(1);
        $modifiedEnsemble->name = '';

        $this->put('/ensemble/1', $modifiedEnsemble->toArray());

        $this->assertResponseCode(400);

        // Make sure it wasn't actually updated.
        $dbEnsemble = $this->EnsembleService->findById(1);
        $this->assertNotEquals('Leeming Concert', $dbEnsemble->name);
    }

    public function testDeleteExists() {
        $this->delete('/ensemble/1');

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->EnsembleService->findById(1);
    }

    public function testDeleteNotExist() {
        $this->delete('/ensemble/0');
        $this->assertResponseCode(404);
    }
}
