<?php

namespace App\Test\TestCase\Controller;

use App\Controller\UsersController;
use App\Test\TestCase\Controller\RestTestCase;
use App\Controller\Component\UsersServiceComponent;
use App\Test\TestCase\DomainObjectFactory;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Controller\ComponentRegistry;
use Cake\I18n\Time;

class UsersControllerTest extends RestTestCase
{
    public $fixtures = [
        'app.Users',
        'app.Role',
        'app.UserRole'
    ];

    public function setUp()
    {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->UsersService = new UsersServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->UsersService);
        parent::tearDown();
    }

    public function testIndex()
    {
        $responseData = $this->get('/users');

        $this->assertResponseOk();
        $this->assertResponseContains('user');

        $this->assertCount(3, $responseData->user);
    }

    public function testViewExists()
    {
        $responseData = $this->get('/users/1');

        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->user->id);
    }

    public function testViewNotExists()
    {
        $this->get('/users/0');

        $this->assertResponseCode(404);
    }

    public function testAdd()
    {
        $newUser = DomainObjectFactory::createUserMap();
        unset($newUser['id']);
        $newUser['username'] = 'newuser';

        $responseData = $this->post('/users', $newUser);

        $this->assertResponseOk();
        $this->assertEquals('newuser', $responseData->user->username);
        $this->assertGreaterThan(0, $responseData->user->id);

        // Make sure it was actually created.
        $this->UsersService->findById($responseData->user->id);
    }

    public function testAddInvalid()
    {
        $newUser = DomainObjectFactory::createUserMap();
        $newUser['created'] = 'notdate';

        $countBefore = $this->UsersService->count();
        $this->post('/users', $newUser);
        $countAfter = $this->UsersService->count();

        $this->assertResponseCode(400);
        $this->assertEquals($countBefore, $countAfter);
    }

    public function testAddDuplicateUsername()
    {
        $newUser = DomainObjectFactory::createUserMap();
        $newUser['username'] = 'test1';

        $countBefore = $this->UsersService->count();
        $this->post('/users', $newUser);
        $countAfter = $this->UsersService->count();

        $this->assertResponseCode(400);
        $this->assertEquals($countBefore, $countAfter);
    }

    public function testEdit()
    {
        $modifiedUser = $this->UsersService->findById(1);
        $modifiedUser->created = new Time('2018-12-12');

        $responseData = $this->put('/users/1', $modifiedUser->toArray());

        $this->assertResponseOk();
        $this->assertEquals('test1', $responseData->user->username);
        $this->assertEquals(1, $responseData->user->id);

        // Make sure it was actually updated.
        $dbAsset = $this->UsersService->findById($responseData->user->id);
        $this->assertEquals(new Time('2018-12-12'), $dbAsset->created);
    }

    public function testEditInvalid()
    {
        $modifiedUser = $this->UsersService->findById(1);
        $modifiedUser->username = 'test2';

        $this->put('/users/1', $modifiedUser->toArray());

        $this->assertResponseCode(400);

        // Make sure it wasn't actually updated.
        $dbAsset = $this->UsersService->findById(1);
        $this->assertNotEquals('test2', $dbAsset->username);
    }

    public function testEditNotExists()
    {
        $modifiedUser = $this->UsersService->findById(1);
        $modifiedUser->created = '2018-12-12';

        $this->put('/users/0', $modifiedUser->toArray());

        $this->assertResponseCode(404);
    }

    public function testDeleteExists()
    {
        $this->delete('/users/1');

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->UsersService->findById(1);
    }

    public function testDeleteNotExist()
    {
        $this->delete('/users/0');

        $this->assertResponseCode(404);
    }

    public function testGetHasNoPassword()
    {
        $responseData = $this->get('/users/1');

        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->user->id);
        $this->assertEquals(false, property_exists($responseData->user, 'password'));
    }
}
