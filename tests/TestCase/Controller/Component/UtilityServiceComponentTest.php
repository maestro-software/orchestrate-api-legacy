<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\UtilityServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

class UtilityServiceComponentTest extends TestCase {
    public $UtilityService;

    public function setUp() {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->UtilityService = new UtilityServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->UtilityService);

        parent::tearDown();
    }

    public function testConvertMapToIdUpdateStructure() {
        $fields = [
            'f1' => 'f2'
        ];

        $entityMap = [
            'f1' => [1, 2, 3]
        ];

        $converted = $this->UtilityService->convertMapToIdUpdateStructure($entityMap, $fields);

        $this->assertEquals($converted['f2']['_ids'], $entityMap['f1']);
    }
}
