<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\EmailServiceComponent;
use App\Controller\Component\MemberServiceComponent;
use App\Controller\Component\SecurityServiceComponent;
use App\Controller\Component\UsersServiceComponent;
use App\Test\TestCase\MockEmailProvider;
use Cake\Cache\Cache;
use Cake\Controller\ComponentRegistry;
use Cake\Core\Configure;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\SecurityServiceComponent Test Case
 */
class SecurityServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Role',
        'app.UserRole',
        'app.Member',
        'app.MemberInstrument',
        'app.Ensemble',
        'app.Concert',
        'app.EnsembleMembership',
        'app.Performance',
        'app.PerformanceMember',
        'app.PerformanceMember',
        'app.Score',
        'app.PerformanceScore',
    ];

    public $SecurityService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->SecurityService = new SecurityServiceComponent($registry);
        $this->MemberService = new MemberServiceComponent($registry);
        $this->UserService = new UsersServiceComponent($registry);
        $this->EmailService = new EmailServiceComponent($registry);
        $this->emailProvider = new MockEmailProvider($this->EmailService);
        $this->SecurityService->EmailService = $this->EmailService;
    }

    public function tearDown()
    {
        unset($this->SecurityService);

        parent::tearDown();
    }

    public function testRequestPasswordReset()
    {
        $member = $this->MemberService->findById(1);

        $this->SecurityService->requestPasswordReset($member->email);

        $email = $this->emailProvider->getLastSend();
        $token = Cache::read($member->email, 'app_password_token');
        $this->assertContains($member->email, $email['toEmail']);
        $this->assertContains("$member->firstName $member->lastName", $email['toName']);
        $this->assertContains($token, $email['content']);
        $this->assertContains(Configure::read('App.frontEndUri'), $email['content']);
    }

    public function testResetPasswordThrowsForNoMember()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');
        $this->SecurityService->resetPassword("invalid@email.com", "", "");
    }

    public function testResetPasswordThrowsForNoToken()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');
        $member = $this->MemberService->findById(1);
        $this->SecurityService->resetPassword($member->email, "", "");
    }

    public function testResetPasswordThrowsForInvalidToken()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');
        $member = $this->MemberService->findById(1);
        Cache::write($member->email, "1234", 'app_password_token');
        $this->SecurityService->resetPassword($member->email, "9876", "");
    }

    public function testResetPasswordDoesNotMeetRequirements()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $member = $this->MemberService->findById(1);
        Cache::write($member->email, "1234", 'app_password_token');
        $this->SecurityService->resetPassword($member->email, "1234", "password");
    }

    public function testResetPasswordDecodesEmail()
    {
        $member = $this->MemberService->findById(1);
        Cache::write($member->email, "1234", 'app_password_token');
        $this->SecurityService->resetPassword(urlencode($member->email), "1234", "somepassword");

        $member = $this->MemberService->findById(1);
        $this->assertNotNull($member->userId);
    }

    public function testResetPasswordCreateNewUser()
    {
        $member = $this->MemberService->findById(3);
        Cache::write($member->email, "1234", 'app_password_token');

        $this->SecurityService->resetPassword($member->email, "1234", "somepassword");

        $member = $this->MemberService->findById(3);
        $this->assertNotNull($member->userId);
        $user = $this->UserService->findById($member->userId);
        $this->assertEquals($member->email, $user->username);
        $this->assertGreaterThan(10, strlen($user->password));
        $this->assertFalse(Cache::read($member->email, 'app_password_token'));
        $this->assertCount(0, $user->roles);
    }

    public function testResetPasswordUpdateExistingUser()
    {
        $member = $this->MemberService->findById(2);
        $user = $this->UserService->create([
            'username' => $member->email,
            'password' => 'abc',
        ]);
        $previousRoleCount = count($user->roles);
        $member = $this->MemberService->update($member->id, ['userId' => $user->id]);
        Cache::write($member->email, "1234", 'app_password_token');

        $this->SecurityService->resetPassword($member->email, "1234", "somepassword");

        $refetchedUser = $this->UserService->findById($member->userId);
        $this->assertNotEquals($user->password, $refetchedUser->password);
        $this->assertFalse(Cache::read($member->email, 'app_password_token'));
        $this->assertCount($previousRoleCount, $refetchedUser->roles);
    }

    public function testInviteMember()
    {
        $member = $this->MemberService->findById(1);

        $this->SecurityService->inviteMemberById(1);

        $email = $this->emailProvider->getLastSend();
        $token = Cache::read($member->email, 'app_password_token');
        $this->assertContains($member->email, $email['toEmail']);
        $this->assertContains("$member->firstName $member->lastName", $email['toName']);
        $this->assertContains($token, $email['content']);
        $this->assertContains(Configure::read('App.frontEndUri'), $email['content']);
    }

    public function testInviteMemberAlreadyHasUser()
    {
        $member = $this->MemberService->findById(1);

        $this->SecurityService->inviteMemberById(1);

        // Email still gets sent
        $email = $this->emailProvider->getLastSend();
        $token = Cache::read($member->email, 'app_password_token');
        $this->assertContains($member->email, $email['toEmail']);
        $this->assertContains("$member->firstName $member->lastName", $email['toName']);
        $this->assertContains($token, $email['content']);
        $this->assertContains(Configure::read('App.frontEndUri'), $email['content']);
    }

    public function testInviteUnknownMember()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->SecurityService->inviteMemberById(0);
    }
}
