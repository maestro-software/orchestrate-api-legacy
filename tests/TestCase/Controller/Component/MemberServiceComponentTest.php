<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\MemberServiceComponent;
use App\Controller\Component\PerformanceMemberServiceComponent;
use App\Controller\Component\EnsembleMembershipServiceComponent;
use App\Controller\Component\MemberInstrumentServiceComponent;
use App\Controller\Component\UsersServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\DomainObjectFactory;
use App\Test\TestCase\CustomAssert;
use Cake\i18n\Time;

class MemberServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Role',
        'app.UserRole',
        'app.Member',
        'app.MemberInstrument',
        'app.Ensemble',
        'app.Concert',
        'app.EnsembleMembership',
        'app.Performance',
        'app.PerformanceMember',
        'app.PerformanceMember',
        'app.Score',
        'app.PerformanceScore'
    ];

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->MemberService = new MemberServiceComponent($registry);
        $this->PerformanceMemberService = new PerformanceMemberServiceComponent($registry);
        $this->EnsembleMembershipService = new EnsembleMembershipServiceComponent($registry);
        $this->MemberInstrumentService = new MemberInstrumentServiceComponent($registry);
        $this->UserService = new UsersServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->MemberService);

        parent::tearDown();
    }

    public function testFindAll()
    {
        $members = $this->MemberService->findAll();

        $this->assertCount(3, $members);
    }

    public function testFindCurrentMembers()
    {
        $members = $this->MemberService->currentMembers();

        $this->assertCount(2, $members);
    }

    public function testFindByIdExists()
    {
        $member = $this->MemberService->findById(1);

        $this->assertEquals(1, $member->id);
    }

    public function testFindByIdNotExists()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->MemberService->findById(0);
    }

    public function testFindByEmailExists()
    {
        $member = $this->MemberService->findbyEmail('bmarley@domain.com');
        $this->assertNotNull($member);
        $this->assertEquals(1, $member->id);
    }

    public function testFindByEmailNotExists()
    {
        $member = $this->MemberService->findbyEmail('notanemail@domain.com');
        $this->assertNull($member);
    }

    public function testCreateValidMember()
    {
        $member = DomainObjectFactory::createMemberMap();

        $countBefore = $this->MemberService->count();
        $this->MemberService->create($member);
        $countAfter = $this->MemberService->count();

        $this->assertEquals($countBefore + 1, $countAfter);
    }

    public function testCreateMemberWithInstruments()
    {
        $memberMap = DomainObjectFactory::createMemberMap();
        $memberMap['instruments'] = ['A', 'B', 'C'];

        $member = $this->MemberService->create($memberMap);
        $instruments = $this->MemberInstrumentService->findAll($member->id);

        $this->assertCount(3, $instruments);
        $this->assertCount(3, $member->instruments);
    }

    public function testCreateMemberWithEnsembles()
    {
        $memberMap = DomainObjectFactory::createMemberMap();
        $memberMap['dateJoined'] = new Time('2017-06-04');
        $memberMap['ensembles'] = [1, 2];

        $member = $this->MemberService->create($memberMap);

        $this->assertCount(2, $member->ensemble);
        $membershipHistory = $this->EnsembleMembershipService->getMembershipHistoryByMemberId($member->id);
        foreach ($membershipHistory as $membership) {
            $this->assertEquals($member->dateJoined, $membership->dateJoined);
        }
    }

    public function testCreateInvalidMember()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $member = DomainObjectFactory::createMemberMap();
        $member['phoneNo'] = '041'; // Must be 12 characters

        $countBefore = $this->MemberService->count();
        $this->MemberService->create($member);
    }

    public function testCreateWithDuplicateEmail()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $existingMember = $this->MemberService->findAll()->first();
        $member = DomainObjectFactory::createMemberMap();
        $member['email'] = $existingMember->email;

        $this->MemberService->create($member);
    }

    public function testUpdateValidMember()
    {
        $member = $this->MemberService->findById(1);
        $member->lastName = 'Geldof';

        $this->MemberService->update(1, $member->toArray());

        $this->assertEquals('Geldof', $this->MemberService->findById(1)->lastName);
    }

    public function testUpdateInvalidMember()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $member = $this->MemberService->findById(1);
        $member->phoneNo = '041'; // Must be 12 characters

        $this->MemberService->update(1, $member->toArray());
    }

    public function testUpdateMemberDateLeftAlsoUpdatesMembership()
    {
        $originalMembershipHistory = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(2)->toArray();
        $memberUpdate = [
            'dateLeft' => new Time('2017-06-04'),
        ];

        $this->MemberService->update(2, $memberUpdate);

        $newMembershipHistory = $this->EnsembleMembershipService->getMembershipHistoryByMemberId(2)->toArray();
        for ($i = 0; $i < count($newMembershipHistory); $i++) {
            $originalDateLeft = $originalMembershipHistory[$i]->dateLeft;
            $dateLeftNow = $newMembershipHistory[$i]->dateLeft;

            if ($originalDateLeft) {
                $this->assertEquals($originalDateLeft, $dateLeftNow);
            } else {
                $this->assertEquals($memberUpdate['dateLeft'], $dateLeftNow);
            }
        }
    }

    public function testUpdateMemberEmailAlsoUpdatesUsernameIfExists()
    {
        $user = $this->UserService->findById(1);
        $member = $this->MemberService->findById(1);
        $this->assertNotEquals($user->username, $member->email);
        $member = $this->MemberService->update($member->id, ['userId' => $user->id]);

        $this->MemberService->update($member->id, ['email' => 'mytest@email.com']);
        $user = $this->UserService->findById(1);
        $this->assertEquals('mytest@email.com', $user->username);
    }

    public function testDeleteMember()
    {
        $this->MemberService->delete(1);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->MemberService->findById(1);
    }

    public function testDeleteUnknown()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->MemberService->delete(0);
    }

    public function testDeleteMemberAlsoDeletesUserIfExists()
    {
        $user = $this->UserService->findById(1);
        $member = $this->MemberService->findById(1);
        $member = $this->MemberService->update($member->id, ['userId' => $user->id]);

        $this->MemberService->delete($member->id);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->UserService->findById($member->userId);
    }

    public function testFindAllHasEmbeddedEnsembles()
    {
        $members = $this->MemberService->findAll();

        foreach ($members as $member) {
            $this->assertNotNull($member->ensemble);
        }
    }

    public function testFindByIdHasEmbeddedEnsembles()
    {
        $member = $this->MemberService->findById(1);
        $this->assertCount(2, $member->ensemble);
    }

    public function testGetCurrentHasEmbeddedEnsembles()
    {
        $members = $this->MemberService->currentMembers();

        foreach ($members as $member) {
            $this->assertNotNull($member->ensemble);
        }
    }

    public function testGetAllMemberSummary()
    {
        $members = $this->MemberService->getAllMembersSummary();
        $this->assertCount(3, $members);

        foreach ($members as $member) {
            CustomAssert::memberHasStandardSummary($member);
        }

        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);
    }

    public function testGetAllMemberSummaryPerformanceCountIsInt()
    {
        $members = $this->MemberService->getAllMembersSummary();

        foreach ($members as $member) {
            $this->assertTrue(is_int($member->performanceCount));
        }
    }

    public function testGetCurrentMemberSummary()
    {
        $members = $this->MemberService->getCurrentMemberSummary();
        $this->assertCount(2, $members);

        foreach ($members as $member) {
            CustomAssert::memberHasStandardSummary($member);
        }

        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);
    }

    public function testGetCurrentMemberSummaryPerformanceCountIsInt()
    {
        $members = $this->MemberService->getCurrentMemberSummary();

        foreach ($members as $member) {
            $this->assertTrue(is_int($member->performanceCount));
        }
    }

    public function testCountsAsSeperateFlagGetsRecognised()
    {
        $members = $this->MemberService->getAllMembersSummary();
        $this->assertCount(3, $members);

        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);
        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 2, 1);
        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 3, 1);
    }

    public function testSeperatePerformanceActuallyMakesADifference()
    {
        $members = $this->MemberService->getAllMembersSummary();
        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);

        $this->PerformanceMemberService->addPerformanceMembership(2, 1);

        $members = $this->MemberService->getAllMembersSummary();
        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 2);
    }

    public function testPerformanceInSameConcertDoesntMakeADifference()
    {
        $members = $this->MemberService->getAllMembersSummary();
        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);

        $this->PerformanceMemberService->addPerformanceMembership(4, 1);

        $members = $this->MemberService->getAllMembersSummary();
        CustomAssert::entityIdHasSpecifiedPerformanceCount($members, 1, 1);
    }

    public function testFindByIdEmbeddedEnsemblesOnlyIncludeActive()
    {
        $member = $this->MemberService->findById(2);
        $this->assertCount(1, $member->ensemble);
    }

    public function testFindAllEmbeddedEnsemblesOnlyIncludeActive()
    {
        $members = $this->MemberService->findAll();
        $this->assertMemberWithIdHasEnsembleCount($members, 2, 1);
    }

    private function assertMemberWithIdHasEnsembleCount($members, $memberId, $ensembleCount)
    {
        foreach ($members as $member) {
            if ($member->id == $memberId) {
                $this->assertCount($ensembleCount, $member->ensemble);
                break;
            }
        }
    }

    public function testGetCurrentEmbeddedEnsemblesOnlyIncludeActive()
    {
        $members = $this->MemberService->currentMembers();
        $this->assertMemberWithIdHasEnsembleCount($members, 2, 1);
    }
}
