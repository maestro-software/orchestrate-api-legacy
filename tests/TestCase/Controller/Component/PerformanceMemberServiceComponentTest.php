<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\PerformanceMemberServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

class PerformanceMemberServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.Ensemble',
        'app.Concert',
        'app.Performance',
        'app.PerformanceMember',
    ];
    public $PerformanceMemberService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->PerformanceMemberService = new PerformanceMemberServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->PerformanceMemberService);
        parent::tearDown();
    }

    public function testFindByPerformanceId()
    {
        $members = $this->PerformanceMemberService->findByPerformanceId(1);
        $this->assertCount(2, $members);
    }

    public function testFindByUnknownPerformanceId()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceMemberService->findByPerformanceId(0);
    }

    public function testFindByPerformanceIdAreActualMembers()
    {
        $members = $this->PerformanceMemberService->findByPerformanceId(1);

        foreach ($members as $member) {
            $this->assertNotNull($member->firstName);
        }
    }

    public function testFindByPerformanceIdDoesntHaveSensitiveData()
    {
        $members = $this->PerformanceMemberService->findByPerformanceId(1);

        foreach ($members as $member) {
            $this->assertNull($member->email);
            $this->assertNull($member->phoneNo);
            $this->assertNull($member->address);
        }
    }

    public function testAddPerformanceMembership()
    {
        $countBefore = $this->countMembersInPerformance(1);
        $this->PerformanceMemberService->addPerformanceMembership(1, 3);
        $countAfter = $this->countMembersInPerformance(1);

        $this->assertEquals($countBefore + 1, $countAfter);
    }

    private function countMembersInPerformance($performanceId)
    {
        $members = $this->PerformanceMemberService->findByPerformanceId($performanceId);
        return $members->count();
    }

    public function testAddAlreadyExistingPerformanceMembership()
    {
        $countBefore = $this->countMembersInPerformance(1);
        $this->PerformanceMemberService->addPerformanceMembership(1, 1);
        $countAfter = $this->countMembersInPerformance(1);

        $this->assertEquals($countBefore, $countAfter);
    }

    public function testDeletePerformanceMembership()
    {
        $countBefore = $this->countMembersInPerformance(1);
        $this->PerformanceMemberService->deletePerformanceMembership(1, 1);
        $countAfter = $this->countMembersInPerformance(1);

        $this->assertEquals($countBefore - 1, $countAfter);
    }

    public function testDeleteNonExistantPerformanceMembership()
    {
        $countBefore = $this->countMembersInPerformance(1);
        $this->PerformanceMemberService->deletePerformanceMembership(1, 3);
        $countAfter = $this->countMembersInPerformance(1);

        $this->assertEquals($countBefore, $countAfter);
    }
}
