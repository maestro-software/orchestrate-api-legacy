<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\ScoreServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use App\Test\TestCase\DomainObjectFactory;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * App\Controller\Component\ScoreServiceComponent Test Case
 */
class ScoreServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Score',
        'app.Concert',
        'app.Ensemble',
        'app.Performance',
        'app.PerformanceScore',
    ];

    private $ScoreService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->ScoreService = new ScoreServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->ScoreService);

        parent::tearDown();
    }

    public function testFindAll()
    {
        $scores = $this->ScoreService->findAll();

        $this->assertCount(3, $scores);
    }

    public function testFindAllHasEmbeddedPlayCount()
    {
        $scores = $this->ScoreService->findAll();
        foreach ($scores as $score) {
            $this->assertNotNull($score->playCount);
        }
    }

    public function testFindByIdExists()
    {
        $score = $this->ScoreService->findById(1);

        $this->assertEquals(1, $score->id);
    }

    public function testFindByIdHasEmbeddedPlayCount()
    {
        $score = $this->ScoreService->findById(1);
        $this->assertNotNull($score->playCount);
    }

    public function testFindByIdNotExists()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $score = $this->ScoreService->findById(0);
    }

    public function testCreateValidScore()
    {
        $score = DomainObjectFactory::createScoreMap();

        $countBefore = $this->ScoreService->count();
        $this->ScoreService->create($score);
        $countAfter = $this->ScoreService->count();

        $this->assertEquals($countBefore + 1, $countAfter);
    }

    public function testCreateInvalidScore()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $score = DomainObjectFactory::createScoreMap();
        $score['title'] = null; // Can't be null

        $this->ScoreService->create($score);
    }

    public function testUpdateValidScore()
    {
        $score = $this->ScoreService->findById(1);
        $score->arranger = 'Steve';

        $this->ScoreService->update(1, $score->toArray());

        $this->assertEquals('Steve', $this->ScoreService->findById(1)->arranger);
    }

    public function testUpdateInvalidScore()
    {
        $this->expectException('Cake\Http\Exception\BadRequestException');

        $score = $this->ScoreService->findById(1);
        $score->title = null;

        $this->ScoreService->update(1, $score->toArray());
    }

    public function testDeleteScore()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');

        $this->ScoreService->delete(1);
        $this->ScoreService->findById(1);
    }

    public function testDeleteUnknown()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->ScoreService->delete(0);
    }
}
