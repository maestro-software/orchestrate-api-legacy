<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\MemberInstrumentServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\MemberInstrumentServiceComponent Test Case
 */
class MemberInstrumentServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.MemberInstrument'
    ];

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->MemberInstrumentService = new MemberInstrumentServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->MemberInstrumentService);

        parent::tearDown();
    }

    public function testFindAll()
    {
        $instruments = $this->MemberInstrumentService->findAll(1);

        $this->assertCount(2, $instruments);
    }

    public function testFindMemberAndInstrument()
    {
        $instrument = $this->MemberInstrumentService->findByMemberAndInstrument(1, 'Clarinet');

        $this->assertNotNull($instrument);
    }

    public function testSetAllInstruments()
    {
        $member = [
            'instruments' => ['Oboe', 'Trumpet', 'Sax']
        ];

        $this->MemberInstrumentService->setMembersInstrumentsFromMemberMap(1, $member);

        $instruments = $this->MemberInstrumentService->findAll(1);
        $this->assertCount(3, $instruments);
    }

    public function testCreateNew()
    {
        $this->MemberInstrumentService->create(3, 'Timpani');

        $instruments = $this->MemberInstrumentService->findAll(3);
        $this->assertCount(1, $instruments);
    }

    public function testCreateAlreadyExists()
    {
        $this->MemberInstrumentService->create(1, 'Clarinet');

        $instruments = $this->MemberInstrumentService->findAll(1);
        $this->assertCount(2, $instruments);
    }

    public function testRemoveMembersInstruments()
    {
        $this->MemberInstrumentService->removeMembersInstruments(1);

        $instruments = $this->MemberInstrumentService->findAll(1);
        $this->assertCount(0, $instruments);
    }

    public function testDeleteSuccess()
    {
        $this->MemberInstrumentService->delete(1, 'Clarinet');

        $instruments = $this->MemberInstrumentService->findAll(1);
        $this->assertCount(1, $instruments);
    }

    public function testDeleteUnknownInstrument()
    {
        $preCount = $this->MemberInstrumentService->findAll(1)->count();
        $this->MemberInstrumentService->delete(1, 'Unknown');
        $this->assertCount($preCount, $this->MemberInstrumentService->findAll(1));
    }

    public function testDeleteUnknownMember()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->MemberInstrumentService->delete(4, 'Clarinet');
    }
}
