<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\RoleServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

class RoleServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Role',
        'app.Users',
        'app.UserRole',
        'app.Ensemble',
        'app.EnsembleRole',
        'app.Member',
        'app.EnsembleMembership',
    ];

    public $RoleService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->RoleService = new RoleServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->RoleService);

        parent::tearDown();
    }

    public function testFindAll()
    {
        $roles = $this->RoleService->findAll();
        $this->assertCount(3, $roles);
    }

    public function testFindByName()
    {
        $roleNames = ['admin', 'editor', 'viewer'];

        foreach ($roleNames as $roleName) {
            $role = $this->RoleService->findByName($roleName);
            $this->assertNotNull($role);
            $this->assertEquals($roleName, $role->role);
        }
    }

    public function testFindByUserId()
    {
        $roles = $this->RoleService->findByUserId(1);
        $this->assertCount(2, $roles);
    }

    public function testFindByUnknownUserId()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->findByUserId(0);
    }

    public function testAddUserToRoleByName()
    {
        $this->RoleService->addUserToRoleByName(1, 'viewer');

        $roles = $this->RoleService->findByUserId(1);
        $this->assertCount(3, $roles);
    }

    public function testAddUserToRoleByNameAlreadyPartOf()
    {
        $this->RoleService->addUserToRoleByName(1, 'admin');

        $roles = $this->RoleService->findByUserId(1);
        $this->assertCount(2, $roles);
    }

    public function testAddUserToRoleByNameNotFound()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->addUserToRoleByName(1, 'notadmin');
    }

    public function testAddUserToRole()
    {
        $this->RoleService->addUserToRole(1, 3);

        $roles = $this->RoleService->findByUserId(1);
        $this->assertCount(3, $roles);
    }

    public function testAddUserToRoleAlreadyPartOf()
    {
        $this->RoleService->addUserToRole(1, 1);

        $roles = $this->RoleService->findByUserId(1);
        $this->assertCount(2, $roles);
    }

    public function testAddUnknownUserToRole()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->addUserToRole(0, 1);
    }

    public function testAddUserToUnknownRole()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->addUserToRole(1, 0);
    }

    public function testRemoveUserFromRole()
    {
        $this->RoleService->removeUserFromRole(1, 1);

        $roles = $this->RoleService->findByUserId(1);
        $this->assertCount(1, $roles);
    }

    public function testRemoveUserFromRoleNotPartOf()
    {
        $this->RoleService->removeUserFromRole(1, 3);

        $roles = $this->RoleService->findByUserId(1);
        $this->assertCount(2, $roles);
    }

    public function testRemoveUnknownUserFromRole()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->removeUserFromRole(0, 1);
    }

    public function testRemoveUserFromUnknownRole()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->removeUserFromRole(1, 0);
    }

    public function testFindByEnsembleId()
    {
        $roles = $this->RoleService->findByEnsembleId(1);
        $this->assertCount(1, $roles);
    }

    public function testFindByUnknownEnsembleId()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->findByUserId(0);
    }

    public function testAddEnsembleToRole()
    {
        $this->RoleService->addEnsembleToRole(1, 3);

        $roles = $this->RoleService->findByEnsembleId(1);
        $this->assertCount(2, $roles);
    }

    public function testAddEnsembleToRoleAlreadyPartOf()
    {
        $this->RoleService->addEnsembleToRole(1, 1);

        $roles = $this->RoleService->findByEnsembleId(1);
        $this->assertCount(1, $roles);
    }

    public function testAddUnknownEnsembleToRole()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->addEnsembleToRole(0, 1);
    }

    public function testAddEnsembleToUnknownRole()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->addEnsembleToRole(1, 0);
    }

    public function testRemoveEnsembleFromRole()
    {
        $this->RoleService->removeEnsembleFromRole(1, 1);

        $roles = $this->RoleService->findByEnsembleId(1);
        $this->assertCount(0, $roles);
    }

    public function testRemoveEnsembleFromRoleNotPartOf()
    {
        $this->RoleService->removeEnsembleFromRole(1, 3);

        $roles = $this->RoleService->findByEnsembleId(1);
        $this->assertCount(1, $roles);
    }

    public function testRemoveUnknownEnsembleFromRole()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->removeEnsembleFromRole(0, 1);
    }

    public function testRemoveEnsembleFromUnknownRole()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->RoleService->removeEnsembleFromRole(1, 0);
    }
}
