<?php

namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\AssetLoanServiceComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;
use \DateTime;

class AssetLoanServiceComponentTest extends TestCase
{
    public $fixtures = [
        'app.Users',
        'app.Member',
        'app.Asset',
        'app.AssetLoan',
        'app.MemberInstrument'
    ];

    public $AssetLoanService;

    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->AssetLoanService = new AssetLoanServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->AssetLoanService);

        parent::tearDown();
    }

    public function testFindByIdExists()
    {
        $loan = $this->AssetLoanService->findById(1);
        $this->assertEquals(1, $loan->id);
        $this->assertEquals(1, $loan->member->id);
        $this->assertEquals(1, $loan->asset->id);
    }

    public function testFindByIdDoesNotExist()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetLoanService->findById(0);
    }

    public function testUpdateDateBorrowed()
    {
        $updateMap = ['dateBorrowed' => new DateTime('+1 day')];

        $loan = $this->AssetLoanService->findById(1);
        $this->assertTrue($loan->dateBorrowed < new DateTime());

        $loan = $this->AssetLoanService->update(1, $updateMap);
        $this->assertTrue($loan->dateBorrowed > new DateTime());
    }

    public function testUpdateDateReturned()
    {
        $updateMap = ['dateReturned' => new DateTime('+1 day')];

        $loan = $this->AssetLoanService->findById(1);
        $this->assertTrue($loan->dateReturned < new DateTime());

        $loan = $this->AssetLoanService->update(1, $updateMap);
        $this->assertTrue($loan->dateReturned > new DateTime());
    }

    public function testUpdateCantUpdateAssetOrMember()
    {
        $updateMap = [
            'assetId' => 3,
            'memberId' => 3
        ];

        $loan = $this->AssetLoanService->update(1, $updateMap);
        $loan = $this->AssetLoanService->findById(1);
        $this->assertNotEquals(3, $loan->assetId);
        $this->assertNotEquals(3, $loan->memberId);
    }

    public function testDeleteLoan()
    {
        $loan = $this->AssetLoanService->findById(1);
        $this->AssetLoanService->delete(1);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetLoanService->findById(1);
    }

    public function testGetHistoryByAssetId()
    {
        $loanHistory = $this->AssetLoanService->getHistoryByAssetId(1);
        $this->assertCount(2, $loanHistory);

        $loanHistory = $this->AssetLoanService->getHistoryByAssetId(2);
        $this->assertCount(1, $loanHistory);

        $loanHistory = $this->AssetLoanService->getHistoryByAssetId(3);
        $this->assertCount(0, $loanHistory);
    }

    public function testGetCurrentLoan()
    {
        $loan = $this->AssetLoanService->getCurrentLoanForAsset(2);

        $this->assertEquals(1, $loan->member->id);
    }

    public function testGetLoanWhenAssetIsNotLoaned()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetLoanService->getCurrentLoanForAsset(1);
    }

    public function testGetHistoryByAssetIdContainsMemberInfo()
    {
        $loanHistory = $this->AssetLoanService->getHistoryByAssetId(1);
        $member = $loanHistory->toArray()[0]->member;

        $this->assertNotNull($member->id);
        $this->assertNotNull($member->firstName);
        $this->assertNotNull($member->lastName);
        $this->assertNull($member->address);
        $this->assertNull($member->phoneNo);
    }

    public function testLoanAssetToMember()
    {
        $loanHistory = $this->AssetLoanService->getHistoryByAssetId(1);
        $this->assertCount(2, $loanHistory);

        $loan = $this->AssetLoanService->loanAssetToMember(1, 1);

        $loanHistory = $this->AssetLoanService->getHistoryByAssetId(1);
        $this->assertCount(3, $loanHistory);
        $this->assertNotNull($loan->dateBorrowed);
        $this->assertNull($loan->dateReturned);
    }

    public function testLoanUnknownAsset()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetLoanService->loanAssetToMember(0, 1);
    }

    public function testLoanAssetToUnknownMember()
    {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetLoanService->loanAssetToMember(1, 0);
    }

    public function testLoanAssetToMemberAlreadyOnLoanToSameMember()
    {
        $this->AssetLoanService->loanAssetToMember(1, 1);
        $loanHistory = $this->AssetLoanService->getHistoryByAssetId(1);
        $this->assertCount(3, $loanHistory);

        // We expect that nothing would happen
        $this->AssetLoanService->loanAssetToMember(1, 1);
        $loanHistory = $this->AssetLoanService->getHistoryByAssetId(1);
        $this->assertCount(3, $loanHistory);
    }

    public function testLoanAssetToMemberAlreadyOnLoanToDifferentMember()
    {
        $this->AssetLoanService->loanAssetToMember(1, 1);

        $this->expectException('Cake\Http\Exception\BadRequestException');
        $this->AssetLoanService->loanAssetToMember(1, 2);
    }

    public function testReturnAssetLoan()
    {
        $loan = $this->AssetLoanService->getCurrentLoanForAsset(2);
        $this->assertNull($loan->dateReturned);

        $loan = $this->AssetLoanService->returnAssetLoan(2);
        $this->assertNotNull($loan->dateReturned);

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->AssetLoanService->getCurrentLoanForAsset(2);
    }

    public function testReturnAssetNotOnLoan()
    {
        $loan = $this->AssetLoanService->returnAssetLoan(1);
        $this->assertNotNull($loan->dateReturned);
    }
}
