<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PerformanceController;
use App\Controller\Component\PerformanceServiceComponent;
use App\Test\TestCase\DomainObjectFactory;

class PerformanceControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();
        $this->PerformanceService = new PerformanceServiceComponent($this->registry);
    }

    public function tearDown() {
        unset($this->PerformanceService);
        parent::tearDown();
    }

    public function testGetPerformances() {
        $response = $this->get('/performance');

        $this->assertResponseOk();
        $this->assertCount(4, $response->performance);
    }

    public function testConcertPerformance() {
        $response = $this->get('/concert/1/performance');

        $this->assertResponseOk();
        $this->assertCount(3, $response->performance);
    }

    public function testIndexUnknownConcert() {
        $this->get('/concert/0/performance');
        $this->assertResponseCode(404);
    }

    public function testView() {
        $response = $this->get('/performance/1');

        $this->assertResponseOk();
        $this->assertEquals(1, $response->performance->id);
        $this->assertNotNull($response->performance->concert);
        $this->assertNotNull($response->performance->ensemble);
    }

    public function testViewNonExistant() {
        $response = $this->get('/performance/0');
        $this->assertResponseCode(404);
    }

    public function testAdd() {
        $newPerformance = DomainObjectFactory::createPerformanceMap();
        $newPerformance['concertId'] = 3;
        $newPerformance['ensembleId'] = 1;

        $response = $this->post('/performance', $newPerformance);

        $this->assertResponseOk();
        $this->assertEquals(3, $response->performance->concertId);
        $this->assertEquals(1, $response->performance->ensembleId);
    }

    public function testAddEmbeddedMembers() {
        $newPerformance = DomainObjectFactory::createPerformanceMap();
        $newPerformance['concertId'] = 3;
        $newPerformance['ensembleId'] = 1;
        $newPerformance['members'] = [1, 2, 3];

        $response = $this->post('/performance', $newPerformance);

        $this->assertResponseOk();
        $this->assertCount(3, $response->performance->member);
    }

    public function testAddUnknownConcert() {
        $newPerformance = DomainObjectFactory::createPerformanceMap();
        $newPerformance['concertId'] = 0;
        $newPerformance['ensembleId'] = 1;

        $this->post('/performance', $newPerformance);
        $this->assertResponseCode(400);
    }

    public function testAddUnknownEnsemble() {
        $newPerformance = DomainObjectFactory::createPerformanceMap();
        $newPerformance['concertId'] = 1;
        $newPerformance['ensembleId'] = 0;

        $this->post('/performance', $newPerformance);
        $this->assertResponseCode(400);
    }

    public function testEditChangeConcert() {
        $concertChange = [
            'concertId' => 3
        ];

        $this->assertNotEquals(3, $this->PerformanceService->findById(1)->concertId);
        $this->put('/performance/1', $concertChange);
        $this->assertResponseOk();
        $this->assertEquals(3, $this->PerformanceService->findById(1)->concertId);
    }

    public function testEditChangeEnsemble() {
        $concertChange = [
            'ensembleId' => 3
        ];

        $this->assertNotEquals(3, $this->PerformanceService->findById(1)->ensembleId);
        $this->put('/performance/1', $concertChange);
        $this->assertResponseOk();
        $this->assertEquals(3, $this->PerformanceService->findById(1)->ensembleId);
    }

    public function testDelete() {
        $this->delete('/performance/1');
        $this->assertResponseOk();

        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->PerformanceService->findById(1);
    }

    public function testDeleteNonExistant() {
        $this->delete('performance/0');
        $this->assertResponseCode(404);
    }
}
