<?php
namespace App\Test\TestCase\Controller;

use App\Controller\ScoreController;
use App\Test\TestCase\Controller\RestTestCase;
use App\Controller\Component\ScoreServiceComponent;
use Cake\Controller\ComponentRegistry;
use App\Test\TestCase\DomainObjectFactory;

class ScoreControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->ScoreService = new ScoreServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->AssetService);
        parent::tearDown();
    }

    public function testIndex() {
        $responseData = $this->get('/score');

        $this->assertResponseOk();
        $this->assertResponseContains('score');

        $this->assertCount(3, $responseData->score);
    }

    public function testViewExists() {
        $responseData = $this->get('/score/1');

        $this->assertResponseOk();
        $this->assertEquals(1, $responseData->score->id);
    }

    public function testViewNotExists() {
        $this->get('/score/0');
        $this->assertResponseCode(404);
    }

    public function testAdd() {
        $newScore = DomainObjectFactory::createScoreMap();
        unset($newScore['id']);
        $newScore['title'] = 'Fur Elise';

        $responseData = $this->post('/score', $newScore);

        $this->assertResponseOk();
        $this->assertEquals('Fur Elise', $responseData->score->title);
        $this->assertGreaterThan(0, $responseData->score->id);

        // Make sure it was actually created.
        $this->ScoreService->findById($responseData->score->id);
    }

    public function testAddInvalid() {
        $newScore = DomainObjectFactory::createScoreMap();
        $newScore['title'] = '';

        $countBefore = $this->ScoreService->count();
        $this->post('/score', $newScore);
        $countAfter = $this->ScoreService->count();

        $this->assertResponseCode(400);
        $this->assertEquals($countBefore, $countAfter);
    }

    public function testEdit() {
        $modifiedScore = $this->ScoreService->findById(1);
        $modifiedScore->title = 'Fur Elise';

        $responseData = $this->put('/score/1', $modifiedScore->toArray());

        $this->assertResponseOk();
        $this->assertEquals('Fur Elise', $responseData->score->title);
        $this->assertEquals(1, $responseData->score->id);

        // Make sure it was actually updated.
        $dbScore = $this->ScoreService->findById($responseData->score->id);
        $this->assertEquals('Fur Elise', $dbScore->title);
    }

    public function testEditInvalid() {
        $modifiedScore = $this->ScoreService->findById(1);
        $modifiedScore->title = '';

        $this->put('/score/1', $modifiedScore->toArray());

        $this->assertResponseCode(400);

        // Make sure it wasn't actually updated.
        $dbScore = $this->ScoreService->findById(1);
        $this->assertNotEquals('Bass Clarinet', $dbScore->title);
    }

    public function testEditNotExists() {
        $modifiedScore = $this->ScoreService->findById(1);
        $modifiedScore->title = 'Fur Elise';

        $this->put('/score/0', $modifiedScore->toArray());

        $this->assertResponseCode(404);
    }

    public function testDeleteExists() {
        $this->expectException('Cake\Datasource\Exception\RecordNotFoundException');
        $this->delete('/score/1');
        $this->ScoreService->findById(1);
    }

    public function testDeleteNotExist() {
        $this->delete('/score/0');
        $this->assertResponseCode(404);
    }
}
