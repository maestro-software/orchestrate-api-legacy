<?php

namespace App\Test\TestCase\Controller;

use App\Controller\Component\RoleServiceComponent;
use Cake\Controller\ComponentRegistry;

class RoleControllerTest extends RestTestCase
{
    public $fixtures = [
        'app.Role',
        'app.Users',
        'app.UserRole',
        'app.Ensemble',
        'app.EnsembleRole',
        'app.Member',
        'app.EnsembleMembership',
    ];

    public function setUp()
    {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->RoleService = new RoleServiceComponent($registry);
    }

    public function tearDown()
    {
        unset($this->RoleService);
        parent::tearDown();
    }

    public function testIndex()
    {
        $responseData = $this->get('/role');
        $this->assertResponseOk();
        $this->assertCount(3, $responseData->role);
    }

    public function testByUserId()
    {
        $responseData = $this->get('/users/1/role');
        $this->assertResponseOk();
        $this->assertCount(2, $responseData->role);
    }

    public function testByUnknownUserId()
    {
        $responseData = $this->get('/users/0/role');
        $this->assertResponseCode(404);
    }

    public function testAddRoleToUser()
    {
        $this->post('/users/1/role/3');
        $this->assertResponseOk();
        $this->assertCount(3, $this->RoleService->findByUserId(1));
    }

    public function testAddRoleToUserAlreadyPartOf()
    {
        $this->post('/users/1/role/1');
        $this->assertResponseOk();
        $this->assertCount(2, $this->RoleService->findByUserId(1));
    }

    public function testAddUnknownRoleToUser()
    {
        $this->post('/users/1/role/0');
        $this->assertResponseCode(404);
    }

    public function testAddRoleToUnknownUser()
    {
        $this->post('/users/0/role/1');
        $this->assertResponseCode(404);
    }

    public function testRemoveRoleFromUser()
    {
        $this->delete('/users/1/role/1');
        $this->assertResponseOk();
        $this->assertCount(1, $this->RoleService->findByUserId(1));
    }

    public function testRemoveRoleFromUserAlreadyRemoved()
    {
        $this->delete('/users/1/role/3');
        $this->assertResponseOk();
        $this->assertCount(2, $this->RoleService->findByUserId(1));
    }

    public function testRemoveUnknownRoleFromUser()
    {
        $this->delete('/users/1/role/0');
        $this->assertResponseCode(404);
    }

    public function testRemoveRoleFromUnknownUser()
    {
        $this->delete('/users/0/role/1');
        $this->assertResponseCode(404);
    }

    public function testByEnsembleId()
    {
        $responseData = $this->get('/ensemble/1/role');
        $this->assertResponseOk();
        $this->assertCount(1, $responseData->role);
    }

    public function testByUnknownEnsembleId()
    {
        $responseData = $this->get('/ensemble/0/role');
        $this->assertResponseCode(404);
    }

    public function testAddRoleToEnsemble()
    {
        $this->post('/ensemble/1/role/3');
        $this->assertResponseOk();
        $this->assertCount(2, $this->RoleService->findByEnsembleId(1));
    }

    public function testAddRoleToEnsembleAlreadyPartOf()
    {
        $this->post('/ensemble/1/role/1');
        $this->assertResponseOk();
        $this->assertCount(1, $this->RoleService->findByEnsembleId(1));
    }

    public function testAddUnknownRoleToEnsemble()
    {
        $this->post('/ensemble/1/role/0');
        $this->assertResponseCode(404);
    }

    public function testAddRoleToUnknownEnsemble()
    {
        $this->post('/ensemble/0/role/1');
        $this->assertResponseCode(404);
    }

    public function testRemoveRoleFromEnsemble()
    {
        $this->delete('/ensemble/1/role/1');
        $this->assertResponseOk();
        $this->assertCount(0, $this->RoleService->findByEnsembleId(1));
    }

    public function testRemoveRoleFromEnsembleAlreadyRemoved()
    {
        $this->delete('/ensemble/1/role/3');
        $this->assertResponseOk();
        $this->assertCount(1, $this->RoleService->findByEnsembleId(1));
    }

    public function testRemoveUnknownRoleFromEnsemble()
    {
        $this->delete('/ensemble/1/role/0');
        $this->assertResponseCode(404);
    }

    public function testRemoveRoleFromUnknownEnsemble()
    {
        $this->delete('/ensemble/0/role/1');
        $this->assertResponseCode(404);
    }
}
