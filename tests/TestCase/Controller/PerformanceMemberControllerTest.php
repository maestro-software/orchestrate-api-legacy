<?php
namespace App\Test\TestCase\Controller;

use App\Controller\PerformanceMemberController;
use App\Controller\Component\PerformanceMemberServiceComponent;
use Cake\Controller\ComponentRegistry;

class PerformanceMemberControllerTest extends RestTestCase {
    public function setUp() {
        parent::setUp();

        $registry = new ComponentRegistry();
        $this->PerformanceMemberService = new PerformanceMemberServiceComponent($registry);
    }

    public function tearDown() {
        unset($this->PerformanceMemberService);
        parent::tearDown();
    }

    public function testIndex() {
        $response = $this->get('/performance/1/member');
        $this->assertCount(2, $response->member);
    }

    public function testIndexNonExistant() {
        $this->get('/performance/0/member');
        $this->assertResponseCode(404);
    }

    public function testAdd() {
        $this->post('/performance/1/member/3');
        $this->assertResponseOk();
        $this->assertNumMembersOfPerformance(3, 1);
    }

    public function testAddAlreadyExists() {
        $this->post('/performance/1/member/1');
        $this->assertResponseOk();
        $this->assertNumMembersOfPerformance(2, 1);
    }

    private function assertNumMembersOfPerformance($numMembers, $performanceId) {
        $members = $this->PerformanceMemberService->findByPerformanceId($performanceId);
        $this->assertCount($numMembers, $members);
    }

    public function testAddToUnknownPerformance() {
        $this->post('performance/0/member/1');
        $this->assertResponseCode(404);
    }

    public function testAddToUnknownMember() {
        $this->post('performance/3/member/0');
        $this->assertResponseCode(404);
    }

    public function testDelete() {
        $this->delete('/performance/1/member/1');
        $this->assertResponseOk();
        $this->assertNumMembersOfPerformance(1, 1);
    }

    public function testDeleteAlreadyDeleted() {
        $this->delete('/performance/1/member/3');
        $this->assertResponseOk();
        $this->assertNumMembersOfPerformance(2, 1);
    }

    public function testDeleteUnknownPerformance() {
        $this->delete('performance/0/member/1');
        $this->assertResponseCode(404);
    }

    public function testDeleteUnknownMember() {
        $this->delete('performance/1/member/0');
        $this->assertResponseCode(404);
    }
}
