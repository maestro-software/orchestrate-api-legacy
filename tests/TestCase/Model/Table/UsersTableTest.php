<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\UsersTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\UsersTable Test Case
 */
class UsersTableTest extends TestCase
{
    public $fixtures = [
        'app.Users'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Users') ? [] : ['className' => 'App\Model\Table\UsersTable'];
        $this->Users = TableRegistry::get('Users', $config);
    }

    public function tearDown()
    {
        unset($this->Users);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $user = $this->Users->get(1);

        $this->assertNotNull($user);
        $this->assertEquals(1, $user->id);
    }

    public function testValidationDefault()
    {
        $user = $this->Users->get(1);
        $user = $this->Users->patchEntity($user, ['password' => null]);

        $this->assertNotEmpty($user->getErrors()['password']);
    }

    public function testPasswordHashedNewUser()
    {
        $user = $this->Users->newEntity(['password' => 'testpassword']);
        $this->assertNotEquals('testpassword', $user->password);
    }

    public function testPasswordHashedExistingUser()
    {
        $user = $this->Users->get(1);
        $user = $this->Users->patchEntity($user, ['password' => 'testpassword']);
        $this->assertNotEquals('testpassword', $user->password);
    }
}
