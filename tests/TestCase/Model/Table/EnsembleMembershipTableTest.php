<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\EnsembleMembershipTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\EnsembleMembershipTable Test Case
 */
class EnsembleMembershipTableTest extends TestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.Users',
        'app.Ensemble',
        'app.Member',
        'app.EnsembleMembership'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('EnsembleMembership') ? [] : ['className' => 'App\Model\Table\EnsembleMembershipTable'];
        $this->EnsembleMembership = TableRegistry::get('EnsembleMembership', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->EnsembleMembership);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $membership = $this->EnsembleMembership->get(1);

        $this->assertNotNull($membership);
        $this->assertEquals(1, $membership->id);
    }
}
