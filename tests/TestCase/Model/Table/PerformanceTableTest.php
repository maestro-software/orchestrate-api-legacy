<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\PerformanceTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class PerformanceTableTest extends TestCase
{
    public $Performance;

    public $fixtures = [
        'app.Ensemble',
        'app.Concert',
        'app.Performance',
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Performance') ? [] : ['className' => 'App\Model\Table\PerformanceTable'];
        $this->Performance = TableRegistry::get('Performance', $config);
    }

    public function tearDown()
    {
        unset($this->Performance);

        parent::tearDown();
    }

    public function testInitialize()
    {
        $performance = $this->Performance->get(1);
        $this->assertEquals(1, $performance->id);
    }
}
