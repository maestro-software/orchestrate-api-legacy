<?php

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ConcertTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

class ConcertTableTest extends TestCase
{
    public $Concert;
    public $fixtures = [
        'app.Concert'
    ];

    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('Concert') ? [] : ['className' => 'App\Model\Table\ConcertTable'];
        $this->Concert = TableRegistry::get('Concert', $config);
    }

    public function tearDown()
    {
        unset($this->Concert);
        parent::tearDown();
    }

    public function testInitialize()
    {
        $concert = $this->Concert->get(1);

        $this->assertNotNull($concert);
        $this->assertEquals(1, $concert->id);
    }

    public function testDateConversionValid()
    {
        $concert = $this->Concert->get(1);
        $data = ['date' => '2015-12-01T12:00Z'];

        $concert = $this->Concert->patchEntity($concert, $data);

        $this->assertInstanceOf('DateTime', $concert['date']);
    }

    public function testDateConversionInValid()
    {
        $concert = $this->Concert->get(1);
        $data = ['date' => '2015-13-01T12:00Z'];

        $this->Concert->patchEntity($concert, $data);

        $this->assertNotNull($concert->getErrors()['date']);
    }
}
