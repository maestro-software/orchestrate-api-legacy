<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * ConcertFixture
 *
 */
class ConcertFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'concert';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'occasion' => ['type' => 'string', 'length' => 90, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'location' => ['type' => 'string', 'length' => 90, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        'date' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'notes' => ['type' => 'string', 'length' => 2000, 'null' => true, 'default' => null, 'collate' => 'latin1_swedish_ci', 'comment' => '', 'precision' => null, 'fixed' => null],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'occasion' => 'State Championships',
            'location' => 'Christchurch Grammer School',
            'date' => '2016-10-08 05:27:29',
            'notes' => 'We won!'
        ],
        [
            'id' => 2,
            'occasion' => 'Halloween Concert',
            'location' => 'Cockburn Youth Centre',
            'date' => '2015-10-30 05:27:29',
            'notes' => 'Spooky'
        ],
        [
            'id' => 3,
            'occasion' => 'Christmas Gig',
            'location' => 'Christchurch Grammer School',
            'date' => '2016-12-13 05:27:29',
            'notes' => 'Ho ho ho'
        ],
    ];
}
