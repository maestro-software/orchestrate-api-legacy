<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * AssetLoanFixture
 *
 */
class AssetLoanFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'asset_loan';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'memberId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'assetId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'dateBorrowed' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        'dateReturned' => ['type' => 'datetime', 'length' => null, 'null' => true, 'default' => null, 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_memberId_idx' => ['type' => 'index', 'columns' => ['memberId'], 'length' => []],
            'fk_assetId_idx' => ['type' => 'index', 'columns' => ['assetId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_assetId' => ['type' => 'foreign', 'columns' => ['assetId'], 'references' => ['asset', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'fk_memberId' => ['type' => 'foreign', 'columns' => ['memberId'], 'references' => ['member', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'memberId' => 1,
            'assetId' => 1,
            'dateBorrowed' => '2016-11-14 05:43:48',
            'dateReturned' => '2016-11-14 05:43:48'
        ],
        [
            'id' => 2,
            'memberId' => 1,
            'assetId' => 2,
            'dateBorrowed' => '2016-11-14 05:43:48',
            'dateReturned' => null
        ],
        [
            'id' => 3,
            'memberId' => 2,
            'assetId' => 1,
            'dateBorrowed' => '2016-10-14 05:43:48',
            'dateReturned' => '2016-10-14 05:43:48'
        ],
    ];
}
