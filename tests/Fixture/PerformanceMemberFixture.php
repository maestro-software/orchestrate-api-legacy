<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PerformanceMemberFixture
 *
 */
class PerformanceMemberFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'performance_member';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'memberId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'performanceId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_performance_member_member_idx' => ['type' => 'index', 'columns' => ['memberId'], 'length' => []],
            'fk_performance_member_performance_idx' => ['type' => 'index', 'columns' => ['performanceId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'uq_ensemble_membership' => ['type' => 'unique', 'columns' => ['memberId', 'performanceId'], 'length' => []],
            'fk_performance_member_member' => ['type' => 'foreign', 'columns' => ['memberId'], 'references' => ['member', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'fk_performance_member_performance' => ['type' => 'foreign', 'columns' => ['performanceId'], 'references' => ['performance', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'memberId' => 1,
            'performanceId' => 1
        ],
        [
            'id' => 2,
            'memberId' => 2,
            'performanceId' => 1
        ],
        [
            'id' => 3,
            'memberId' => 3,
            'performanceId' => 2
        ],
    ];
}
