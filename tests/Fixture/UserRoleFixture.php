<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * UserRoleFixture
 *
 */
class UserRoleFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'user_role';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'userId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'roleId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_user_role_user_idx' => ['type' => 'index', 'columns' => ['userId'], 'length' => []],
            'fk_user_role_role_idx' => ['type' => 'index', 'columns' => ['roleId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_user_role_role' => ['type' => 'foreign', 'columns' => ['roleId'], 'references' => ['role', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'fk_user_role_user' => ['type' => 'foreign', 'columns' => ['userId'], 'references' => ['users', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        // Admin and editor
        [
            'id' => 1,
            'userId' => 1,
            'roleId' => 1
        ],
        [
            'id' => 2,
            'userId' => 1,
            'roleId' => 2
        ],
        // Just editor
        [
            'id' => 3,
            'userId' => 2,
            'roleId' => 2
        ],
        // User
        [
            'id' => 4,
            'userId' => 3,
            'roleId' => 3
        ],
    ];
}
