<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PerformanceScoreFixture
 *
 */
class PerformanceScoreFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'performance_score';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'performanceId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'scoreId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        '_indexes' => [
            'fk_performance_scores_performance_idx' => ['type' => 'index', 'columns' => ['performanceId'], 'length' => []],
            'fk_performance_scores_score_idx' => ['type' => 'index', 'columns' => ['scoreId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'uq_performance_score' => ['type' => 'unique', 'columns' => ['performanceId', 'scoreId'], 'length' => []],
            'fk_performance_scores_performance' => ['type' => 'foreign', 'columns' => ['performanceId'], 'references' => ['performance', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'fk_performance_scores_score' => ['type' => 'foreign', 'columns' => ['scoreId'], 'references' => ['score', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'performanceId' => 1,
            'scoreId' => 1
        ],
        [
            'id' => 2,
            'performanceId' => 1,
            'scoreId' => 2
        ],
        [
            'id' => 3,
            'performanceId' => 2,
            'scoreId' => 1
        ],
    ];
}
