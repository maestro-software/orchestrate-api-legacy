<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * PerformanceFixture
 *
 */
class PerformanceFixture extends TestFixture
{

    /**
     * Table name
     *
     * @var string
     */
    public $table = 'performance';

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'autoIncrement' => true, 'precision' => null],
        'ensembleId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'concertId' => ['type' => 'integer', 'length' => 11, 'unsigned' => false, 'null' => false, 'default' => null, 'comment' => '', 'precision' => null, 'autoIncrement' => null],
        'countsAsSeperate' => ['type' => 'boolean', 'length' => null, 'null' => false, 'default' => '1', 'comment' => '', 'precision' => null],
        '_indexes' => [
            'fk_ensembleName_idx' => ['type' => 'index', 'columns' => ['ensembleId'], 'length' => []],
            'fk_performance_concert_idx' => ['type' => 'index', 'columns' => ['concertId'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'fk_ensemble_performance' => ['type' => 'foreign', 'columns' => ['ensembleId'], 'references' => ['ensemble', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
            'fk_performance_concert' => ['type' => 'foreign', 'columns' => ['concertId'], 'references' => ['concert', 'id'], 'update' => 'noAction', 'delete' => 'cascade', 'length' => []],
        ],
        '_options' => [
            'engine' => 'InnoDB',
            'collation' => 'latin1_swedish_ci'
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'ensembleId' => 1,
            'concertId' => 1,
            'countsAsSeperate' => false
        ],
        [
            'id' => 2,
            'ensembleId' => 2,
            'concertId' => 1,
            'countsAsSeperate' => true
        ],
        [
            'id' => 3,
            'ensembleId' => 1,
            'concertId' => 2,
            'countsAsSeperate' => false
        ],
        [
            'id' => 4,
            'ensembleId' => 3,
            'concertId' => 1,
            'countsAsSeperate' => false
        ],
    ];
}
