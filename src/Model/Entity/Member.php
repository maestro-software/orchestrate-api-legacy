<?php

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Member Entity.
 *
 * @property int $id
 * @property string $firstName
 * @property string $lastName
 * @property string $phoneNo
 * @property string $address
 * @property string $membershipType
 * @property bool $isCurrent
 * @property string $notes
 * @property \App\Model\Entity\Ensemble[] $ensemble
 * @property \App\Model\Entity\Performance[] $performance
 */
class Member extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
