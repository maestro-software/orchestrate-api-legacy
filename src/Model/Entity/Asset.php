<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Asset Entity.
 *
 * @property int $id
 * @property string $description
 * @property string $serialNo
 * @property \Cake\I18n\Time $dateAcquired
 * @property float $valuePaid
 * @property \Cake\I18n\Time $dateDiscarded
 * @property float $valueDiscarded
 * @property float $insuranceValue
 * @property string $currentLocation
 * @property string $notes
 */
class Asset extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false,
    ];
}
