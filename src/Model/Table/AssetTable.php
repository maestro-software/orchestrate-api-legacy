<?php
namespace App\Model\Table;

use App\Model\Entity\Asset;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use \ArrayObject;
use Cake\Event\Event;

/**
 * Asset Model
 *
 */
class AssetTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('AutoDateConvert', [
            'dateTimeFields' => ['dateAcquired', 'dateDiscarded']
        ]);

        $this->setTable('asset');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasOne('AssetLoan', [
            'foreignKey' => 'assetId',
            'dependent' => true,
            'propertyName' => 'loanedTo',
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->add('id', 'valid', ['rule' => 'numeric'])
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        $validator
            ->add('quantity', 'valid', ['rule' => 'naturalNumber'])
            ->allowEmpty('quantity');

        $validator
            ->allowEmpty('serialNo');

        $validator
            ->add('dateAcquired', 'valid', ['rule' => 'datetime'])
            ->requirePresence('dateAcquired', 'create')
            ->notEmpty('dateAcquired');

        $validator
            ->add('valuePaid', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('valuePaid');

        $validator
            ->add('dateDiscarded', 'valid', ['rule' => 'datetime'])
            ->allowEmpty('dateDiscarded');

        $validator
            ->add('valueDiscarded', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('valueDiscarded');

        $validator
            ->add('insuranceValue', 'valid', ['rule' => 'decimal'])
            ->allowEmpty('insuranceValue');

        $validator
            ->allowEmpty('currentLocation');

        $validator
            ->allowEmpty('notes');

        return $validator;
    }
}
