<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * PerformanceMember Model
 *
 * @method \App\Model\Entity\PerformanceMember get($primaryKey, $options = [])
 * @method \App\Model\Entity\PerformanceMember newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\PerformanceMember[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\PerformanceMember|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\PerformanceMember patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\PerformanceMember[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\PerformanceMember findOrCreate($search, callable $callback = null)
 */
class PerformanceMemberTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('performance_member');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Member', [
            'foreignKey' => 'memberId'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->integer('memberId')
            ->requirePresence('memberId', 'create')
            ->notEmpty('memberId');

        $validator
            ->integer('performanceId')
            ->requirePresence('performanceId', 'create')
            ->notEmpty('performanceId');

        return $validator;
    }
}
