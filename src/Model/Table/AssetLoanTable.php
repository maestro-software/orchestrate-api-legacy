<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * AssetLoan Model
 *
 * @method \App\Model\Entity\AssetLoan get($primaryKey, $options = [])
 * @method \App\Model\Entity\AssetLoan newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\AssetLoan[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\AssetLoan|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\AssetLoan patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\AssetLoan[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\AssetLoan findOrCreate($search, callable $callback = null)
 */
class AssetLoanTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->addBehavior('AutoDateConvert', [
            'dateTimeFields' => ['dateBorrowed', 'dateReturned']
        ]);

        $this->setTable('asset_loan');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Member', [
            'foreignKey' => 'memberId'
        ]);
        $this->belongsTo('Asset', [
            'foreignKey' => 'assetId'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create')
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table']);

        $validator
            ->integer('memberId')
            ->requirePresence('memberId', 'create')
            ->notEmpty('memberId');

        $validator
            ->integer('assetId')
            ->requirePresence('assetId', 'create')
            ->notEmpty('assetId');

        $validator
            ->dateTime('dateBorrowed')
            ->allowEmpty('dateBorrowed');

        $validator
            ->dateTime('dateReturned')
            ->allowEmpty('dateReturned');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['id']));

        return $rules;
    }
}
