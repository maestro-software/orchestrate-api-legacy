<?php
namespace App\Controller;

use App\Controller\AppController;

class AssetLoanController extends AppController {
    public $components = [
        'AssetLoanService'
    ];

    public function view($id = null) {
        $assetLoan = $this->AssetLoanService->findById($id);

        $this->set('assetLoan', $assetLoan);
        $this->set('_serialize', ['assetLoan']);
    }

    public function edit($id = null) {
        $assetLoan = $this->AssetLoanService->update($id, $this->request->getData());

        $this->set('assetLoan', $assetLoan);
        $this->set('_serialize', ['assetLoan']);
    }

    public function delete($id = null) {
        $this->AssetLoanService->delete($id);
    }

    public function byAssetId() {
        $assetId = $this->request->getParam('asset_id');
        $assetLoan = $this->AssetLoanService->getHistoryByAssetId($assetId);

        $this->set('assetLoan', $assetLoan);
        $this->set('_serialize', ['assetLoan']);
    }

    public function current() {
        $assetId = $this->request->getParam('asset_id');
        $assetLoan = $this->AssetLoanService->getCurrentLoanForAsset($assetId);

        $this->set('assetLoan', $assetLoan);
        $this->set('_serialize', ['assetLoan']);
    }

    public function loanToMember() {
        $assetId = $this->request->getParam('asset_id');
        $memberId = $this->request->getParam('member_id');

        $assetLoan = $this->AssetLoanService->loanAssetToMember($assetId, $memberId);

        $this->set('assetLoan', $assetLoan);
        $this->set('_serialize', ['assetLoan']);
    }

    public function returnAsset() {
        $assetId = $this->request->getParam('asset_id');

        $assetLoan = $this->AssetLoanService->returnAssetLoan($assetId);

        $this->set('assetLoan', $assetLoan);
        $this->set('_serialize', ['assetLoan']);
    }
}
