<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Ensemble Controller
 *
 * @property \App\Model\Table\EnsembleTable $Ensemble
 */
class EnsembleController extends AppController
{
    /* Automatically include the Ensemble Service */
    public $components = [
      'EnsembleService'
    ];

    /**
     * Index method
     *
     * @return void
     */
    public function index()
    {
        $ensembles = $this->EnsembleService->findAll();

        $this->set('ensemble', $ensembles);
        $this->set('_serialize', ['ensemble']);
    }

    /**
     * View method
     *
     * @param string|null $id Ensemble id.
     * @return void
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function view($id = null)
    {
        $ensemble = $this->EnsembleService->findById($id);

        $this->set('ensemble', $ensemble);
        $this->set('_serialize', ['ensemble']);
    }

    /**
     * Add method
     *
     * @return void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->request->allowMethod(['post']);

        $ensemble = $this->EnsembleService->create($this->request->getData());

        $this->set('ensemble', $ensemble);
        $this->set('_serialize', ['ensemble']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Ensemble id.
     * @return void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->request->allowMethod(['put']);

        $ensemble = $this->EnsembleService->update($id, $this->request->getData());

        $this->set('ensemble', $ensemble);
        $this->set('_serialize', ['ensemble']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Ensemble id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Http\Exception\NotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['delete']);

        $this->EnsembleService->delete($id);
    }
}
