<?php
namespace App\Controller;

use App\Controller\AppController;

class PerformanceScoreController extends AppController {
    public $components = [
        'PerformanceScoreService'
    ];

    public function index() {
        $performanceId = $this->request->getParam('performance_id');
        $scores = $this->PerformanceScoreService->findByPerformanceId($performanceId);

        $this->set('score', $scores);
        $this->set('_serialize', ['score']);
    }

    public function add() {
        $scoreId = $this->request->getParam('score_id');
        $performanceId = $this->request->getParam('performance_id');

        $this->PerformanceScoreService->addPerformanceScore($performanceId, $scoreId);
    }

    public function delete() {
        $scoreId = $this->request->getParam('score_id');
        $performanceId = $this->request->getParam('performance_id');

        $this->PerformanceScoreService->deletePerformanceScore($performanceId, $scoreId);
    }
}
