<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * EnsembleMembership Controller
 *
 * @property \App\Model\Table\EnsembleMembershipTable $EnsembleMembership
 */
class EnsembleMembershipController extends AppController
{
    public $components = [
        'EnsembleMembershipService'
    ];

    public function index() {
        $ensembleId = $this->request->getParam('ensemble_id');
        $members = $this->EnsembleMembershipService->getMembersByEnsembleId($ensembleId);

        $this->set('members', $members);
        $this->set('_serialize', ['members']);
    }

    public function currentMembersInEnsemble() {
        $ensembleId = $this->request->getParam('ensemble_id');
        $members = $this->EnsembleMembershipService->getCurrentMembersByEnsembleId($ensembleId);

        $this->set('members', $members);
        $this->set('_serialize', ['members']);
    }

    public function membershipHistoryForMember() {
        $memberId = $this->request->getParam('member_id');

        $history = $this->EnsembleMembershipService->getMembershipHistoryByMemberId($memberId);

        $this->set('membershipHistory', $history);
        $this->set('_serialize', ['membershipHistory']);
    }

    public function pastMembersInEnsemble() {
        $ensembleId = $this->request->getParam('ensemble_id');
        $members = $this->EnsembleMembershipService->getPastMembersByEnsembleId($ensembleId);

        $this->set('members', $members);
        $this->set('_serialize', ['members']);
    }

    public function addMemberToEnsemble() {
        $this->request->allowMethod(['post']);

        $membership = [
            'ensembleId' => $this->request->getParam('ensemble_id'),
            'memberId' => $this->request->getParam('member_id')
        ];

        $membership = $this->EnsembleMembershipService->addMembership($membership);

        $this->serialisePropertyAndData('membership', $membership);
    }

    public function removeMemberFromEnsemble() {
        $this->request->allowMethod(['post']);

        $memberId = $this->request->getParam('member_id');
        $ensembleId = $this->request->getParam('ensemble_id');

        $this->EnsembleMembershipService->endMembership($memberId, $ensembleId);
    }

    public function view($membershipId) {
        $membership = $this->EnsembleMembershipService->findById($membershipId);

        $this->set('membership', $membership);
        $this->set('_serialize', ['membership']);
    }

    public function edit($membershipId) {
        $membership = $this->EnsembleMembershipService->update($membershipId, $this->request->getData());

        $this->set('membership', $membership);
        $this->set('_serialize', ['membership']);
    }

    public function delete($membershipId) {
        $this->request->allowMethod(['delete']);

        $this->EnsembleMembershipService->deleteMembershipById($membershipId);
    }
}
