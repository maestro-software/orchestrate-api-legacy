<?php

namespace App\Controller;

use App\Controller\AppController;
use Cake\Event\Event;
use Cake\Http\Exception\UnauthorizedException;

class SecurityController extends AppController
{
    public $components = [
        'UsersService',
        'SecurityService',
    ];

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);

        // Allow users to logout.
        // You should not add the "login" action to allow list. Doing so would
        // cause problems with normal functioning of AuthComponent.
        $this->Auth->allow([
            'logout',
            'getCsrfToken',
            'getCurrentUser',
            'requestPasswordReset',
            'resetPassword',
        ]);
    }

    public function isAuthorized($user)
    {
        // Allow anyone access to these methods (unauthenticated)
        return true;
    }

    public function login()
    {
        $this->request->allowMethod(['post']);

        // Get CakePHP to check for valid user credentials (in our case
        // as JSON data in the POST request).
        $user = $this->Auth->identify();

        if (!$user) {
            throw new UnauthorizedException('Incorrect Username or Password');
        }

        // Get the full user object (which includes roles)
        $user = $this->UsersService->findByIdForAuthorisation($user['id']);

        // When a valid user identified, add the user to the session storage.
        $this->Auth->setUser($user);

        // Return the user that has logged in (for front end permissions).
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function logout()
    {
        $this->request->allowMethod(['post']);

        // Stop CakePHP from looking for a view file  (i.e. .ctp OR .php)
        $this->autoRender = false;

        $this->Auth->logout();
    }

    public function getCurrentUser()
    {
        $this->request->allowMethod(['get']);

        // Get the currently logged in user.
        $user = $this->Auth->user();

        if (!$user) {
            throw new UnauthorizedException();
        }

        // Return the user that is logged in (for front end permissions).
        $this->set('user', $user);
        $this->set('_serialize', ['user']);
    }

    public function getCsrfToken()
    {
        $this->request->allowMethod(['get']);

        $csrfToken = $this->request->getParam('_csrfToken');

        $this->set('csrfToken', $csrfToken);
        $this->set('_serialize', ['csrfToken']);
    }

    public function requestPasswordReset()
    {
        $data = $this->request->getData();
        $this->SecurityService->requestPasswordReset($data['email']);
    }

    public function resetPassword()
    {
        $data = $this->request->getData();
        $this->SecurityService->resetPassword($data['email'], $data['token'], $data['password']);
    }
}
