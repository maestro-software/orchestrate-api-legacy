<?php
namespace App\Controller;

use App\Controller\AppController;

class RoleController extends AppController
{
    public $components = [
        'RoleService',
    ];

    public function isAuthorized($user)
    {
        return $this->AuthorisationService->userHasRole($user, 'admin');
    }

    public function index()
    {
        $roles = $this->RoleService->findAll();

        $this->set('role', $roles);
        $this->set('_serialize', ['role']);
    }

    public function byUserId()
    {
        $userId = $this->request->getParam('user_id');
        $roles = $this->RoleService->findByUserId($userId);

        $this->set('role', $roles);
        $this->set('_serialize', ['role']);
    }

    public function addUser()
    {
        $userId = $this->request->getParam('user_id');
        $roleId = $this->request->getParam('role_id');

        $this->RoleService->addUserToRole($userId, $roleId);
    }

    public function removeUser()
    {
        $userId = $this->request->getParam('user_id');
        $roleId = $this->request->getParam('role_id');

        $this->RoleService->removeUserFromRole($userId, $roleId);
    }

    public function byEnsembleId()
    {
        $ensembleId = $this->request->getParam('ensemble_id');
        $roles = $this->RoleService->findByEnsembleId($ensembleId);

        $this->set('role', $roles);
        $this->set('_serialize', ['role']);
    }

    public function addEnsemble()
    {
        $ensembleId = $this->request->getParam('ensemble_id');
        $roleId = $this->request->getParam('role_id');

        $this->RoleService->addEnsembleToRole($ensembleId, $roleId);
    }

    public function removeEnsemble()
    {
        $ensembleId = $this->request->getParam('ensemble_id');
        $roleId = $this->request->getParam('role_id');

        $this->RoleService->removeEnsembleFromRole($ensembleId, $roleId);
    }
}
