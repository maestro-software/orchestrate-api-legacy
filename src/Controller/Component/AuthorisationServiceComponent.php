<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

class AuthorisationServiceComponent extends Component
{
    public function requestAndUserMatchesStandardAuthorisation($request, $user)
    {
        // By default allow admins and editors to access everything, allow viewers only to read
        return $this->userHasRole($user, 'admin') ||
            $this->userHasRole($user, 'editor') ||
            $this->requestIsViewerAttemptingToRead($request, $user);
    }

    public function userHasRole($user, $role)
    {
        if (isset($user['roles'])) {
            foreach ($user['roles'] as $userRole) {
                if ($userRole['role'] == $role) {
                    return true;
                }
            }
        }

        if (isset($user['member'])) {
            foreach ($user['member']['ensemble'] as $ensemble) {
                foreach ($ensemble['roles'] as $ensembleRole) {
                    if ($ensembleRole['role'] == $role) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function requestIsViewerAttemptingToRead($request, $user)
    {
        return $this->userHasRole($user, 'viewer') && $request->is('get');
    }
}
