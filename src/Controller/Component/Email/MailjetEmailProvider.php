<?php

namespace App\Controller\Component\Email;

use App\Controller\Component\Email\EmailProvider;
use Cake\Core\Configure;
use Mailjet\Resources;

class MailjetEmailProvider implements EmailProvider
{
    private $mailjet;

    public function __construct()
    {
        $publicKey = Configure::read('App.mailjetPublicKey');
        $secretKey = Configure::read('App.mailjetSecretKey');
        $this->mailjet = new \Mailjet\Client($publicKey, $secretKey, true, ['version' => 'v3.1']);
    }

    public function send($emailData)
    {
        $body = [
            'Messages' => [
                [
                    'From' => [
                        'Email' => Configure::read('App.defaultFromEmail'),
                        'Name' => Configure::read('App.organisationName')
                    ],
                    'To' => [
                        [
                            'Email' => $emailData['toEmail'],
                            'Name' => $emailData['toName']
                        ]
                    ],
                    'Subject' => $emailData['subject'],
                    'TextPart' => $emailData['content'],
                ]
            ]
        ];
        $response = $this->mailjet->post(Resources::$Email, ['body' => $body]);
    }
}
