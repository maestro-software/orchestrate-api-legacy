<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;

class ConcertServiceComponent extends Component
{
    protected $_defaultConfig = [];

    private $concertTable;

    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->concertTable = TableRegistry::get('Concert');
    }

    public function findAll()
    {
        return $this->concertTable->find();
    }

    public function findById($id)
    {
        return $this->concertTable->get($id);
    }

    public function create($concertMap)
    {
        $concert = $this->concertTable->newEntity($concertMap);
        $this->saveConcert($concert);

        return $concert;
    }

    private function saveConcert($concert)
    {
        if ($concert->getErrors()) {
            throw new BadRequestException($concert->getErrors());
        }

        if (!$this->concertTable->save($concert)) {
            throw new InternalErrorException("Unable to save concert");
        }
    }

    public function update($id, $concertMap)
    {
        $concert = $this->findById($id);
        $concert = $this->concertTable->patchEntity($concert, $concertMap);
        $this->saveConcert($concert);

        return $concert;
    }

    public function delete($id)
    {
        $concert = $this->findById($id);

        if (!$this->concertTable->delete($concert)) {
            throw new InternalErrorException("Unable to delete concert");
        }
    }

    public function count()
    {
        return $this->findAll()->count();
    }

    public function findByMemberId($memberId)
    {
        /* A bit nasty, return all concerts (and performances) member is involved in, embedding
           the ensemble the performance is for */
        $concerts = $this->concertTable->find()
            ->contain(['Performance' => function ($q) use ($memberId) {
                return $q->innerJoinWith('Member', function ($q) use ($memberId) {
                    return $q->where(['Member.id' => $memberId]);
                });
            }, 'Performance.Ensemble'])
            ->innerJoinWith('Performance.Member', function ($q) use ($memberId) {
                return $q->where(['Member.id' => $memberId]);
            })
            ->distinct();

        return $concerts;
    }
}
