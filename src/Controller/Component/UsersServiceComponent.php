<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Cache\Cache;
use Cake\I18n\Time;
use Cake\ORM\Query;

class UsersServiceComponent extends Component
{
    public $components = [
        'EmailService',
    ];

    private $usersTable;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->usersTable = TableRegistry::get('Users');
    }

    public function findAll()
    {
        return $this->usersTable->find()
            ->contain(['Role']);
    }

    public function findById($id = null)
    {
        return $this->usersTable->get($id, [
            'contain' => ['Role']
        ]);
    }

    public function findByIdForAuthorisation(int $id)
    {
        return $this->usersTable->get($id, [
            'contain' => [
                'Role',
                'Member.Ensemble' => function (Query $q) {
                    return $q->where([
                        'OR' => [
                            'EnsembleMembership.dateLeft IS NULL',
                            'EnsembleMembership.dateLeft >' => new Time(),
                        ]
                    ])
                        ->contain('Role');
                },
            ],
        ]);
    }

    public function findByUsername(string $username)
    {
        return $this->usersTable->find()
            ->where(['username' => $username])
            ->first();
    }

    public function create($userMap = null)
    {
        // Attempt to map the map to a user
        $user = $this->usersTable->newEntity($userMap);

        if ($user->getErrors()) {
            throw new BadRequestException($user->getErrors());
        }

        if (!$this->usersTable->save($user)) {
            throw new InternalErrorException("Unable to save user.");
        }

        return $this->findById($user->id);
    }

    public function update($id = null, $userMap = null)
    {
        // Attempt to map the map to an user
        $user = $this->findById($id);

        try {
            $user = $this->usersTable->patchEntity($user, $userMap);
        } catch (\Exception $e) {
            throw new BadRequestException($e->getMessage());
        }

        if ($user->getErrors()) {
            throw new BadRequestException($user->getErrors());
        }

        if (!$this->usersTable->save($user)) {
            throw new InternalErrorException("Unable to save user.");
        }

        return $this->findById($id);
    }

    public function delete($id = null)
    {
        $user = $this->findById($id);

        if (!$user) {
            return;
        }

        if (!$this->usersTable->delete($user)) {
            throw new BadRequestException("Couldn't delete user.");
        }
    }

    public function count()
    {
        return $this->usersTable->find()->count();
    }
}
