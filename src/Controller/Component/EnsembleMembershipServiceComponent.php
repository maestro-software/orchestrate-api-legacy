<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\I18n\Time;

class EnsembleMembershipServiceComponent extends Component
{
    public $components = [
        'MemberService',
        'UtilityService'
    ];

    private $ensembleMembershipTable;

    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->ensembleMembershipTable = TableRegistry::get('EnsembleMembership');
    }

    public function findById($ensembleMembershipId)
    {
        return $this->ensembleMembershipTable->get($ensembleMembershipId);
    }

    public function getMembershipHistoryByMemberId($memberId)
    {
        return $this->getMembershipHistoryForMemberId($memberId)
            ->contain(['Ensemble']);
    }

    public function getCurrentMembersByEnsembleId($ensembleId)
    {
        return $this->getMembersByEnsembleId($ensembleId)
            ->where(['OR' => [
                'EnsembleMembership.dateLeft IS NULL',
                'EnsembleMembership.dateLeft >' => new Time(),
            ]]);
    }

    public function getPastMembersByEnsembleId($ensembleId)
    {
        return $this->getMembersByEnsembleId($ensembleId)
            ->where(['EnsembleMembership.dateLeft <' => new Time()]);
    }

    public function getMembersByEnsembleId($ensembleId)
    {
        return $this->ensembleMembershipTable->find('all')
            ->contain(['Member' => function ($q) {
                return $q->select(['id', 'firstName', 'lastName']);
            }])
            ->where(['EnsembleMembership.ensembleId' => $ensembleId])
            ->formatResults(function ($results) {
                return $results->map(function ($row) {
                    return $row['member'];
                });
            });
    }

    public function addMembership($membershipMap)
    {
        $membership = $this->findMembership($membershipMap['memberId'], $membershipMap['ensembleId']);

        if (!$membership) {
            $membership = $this->ensembleMembershipTable->newEntity($membershipMap);

            if (!$membership->dateJoined) {
                $membership->dateJoined = new Time();
            }

            $this->saveMembership($membership);
            $this->removeMemberDateLeftIfSet($membership->memberId);
        }

        return $membership;
    }

    private function removeMemberDateLeftIfSet($memberId)
    {
        $member = $this->MemberService->findById($memberId);

        if ($member->dateLeft != null) {
            $memberUpdate = [
                'dateLeft' => null
            ];

            $this->MemberService->update($memberId, $memberUpdate);
        }
    }

    public function findMembership($memberId, $ensembleId)
    {
        return $this->ensembleMembershipTable->find('all')
            ->where([
                'EnsembleMembership.memberId' => $memberId,
                'EnsembleMembership.ensembleId' => $ensembleId,
                'OR' => [
                    'EnsembleMembership.dateLeft IS NULL',
                    'EnsembleMembership.dateLeft >' => new Time(),
                ]
            ])
            ->first();
    }

    public function endMembership($memberId, $ensembleId)
    {
        $membership = $this->findMembership($memberId, $ensembleId);

        if ($membership) {
            $membership->dateLeft = new Time();
            $this->saveMembership($membership);

            $this->setMemberDateLeftIfNoActiveMemberships($membership->memberId, $membership->dateLeft);
        }
    }

    private function setMemberDateLeftIfNoActiveMemberships($memberId, $dateLeft)
    {
        $allMembership = $this->getMembershipHistoryForMemberId($memberId);
        $noActiveMemberships = $this->UtilityService->array_all($allMembership, function ($membership) {
            return !!$membership->dateLeft;
        });

        if ($noActiveMemberships) {
            $memberUpdate = [
                'dateLeft' => $dateLeft
            ];

            $this->MemberService->update($memberId, $memberUpdate);
        }
    }

    public function update($membershipId, $membershipMap)
    {
        $membership = $this->findById($membershipId);

        // Date Left cannot be set to null here, delete the membership and create another
        // one instead
        if (isset($membershipMap['dateJoined'])) {
            $membership->dateJoined = $membershipMap['dateJoined'];
        }
        if (isset($membershipMap['dateLeft'])) {
            $membership->dateLeft = $membershipMap['dateLeft'];
        }

        $this->saveMembership($membership);

        return $this->findById($membershipId);
    }

    private function saveMembership($membership)
    {
        $errors = $membership->getErrors();

        if ($errors) {
            throw new BadRequestException($errors);
        }

        if (!$this->ensembleMembershipTable->save($membership)) {
            throw new InternalErrorException("Unable to save membership");
        }
    }

    public function deleteMembershipById($membershipId)
    {
        $membership = $this->findById($membershipId);

        if (!$this->ensembleMembershipTable->delete($membership)) {
            throw new InternalErrorException('Failed to delete loan');
        }
    }

    public function setMemberIdsMembershipToJoinDate($memberId, $joinDate)
    {
        if (!$joinDate) {
            throw new BadRequestException('Invalid joinDate passed');
        }

        $allMembership = $this->getMembershipHistoryForMemberId($memberId);

        foreach ($allMembership as $membership) {
            $membership->dateJoined = $joinDate;
            $this->saveMembership($membership);
        }
    }

    public function setMemberIdsMembershipToLeftDate($memberId, $newLeftDate)
    {
        if (!isset($newLeftDate)) {
            return;
        }

        $member = $this->MemberService->findById($memberId);
        $allMembership = $this->getMembershipHistoryForMemberId($memberId);

        foreach ($allMembership as $membership) {
            $existingDateLeft = $membership->dateLeft;

            if (!isset($existingDateLeft)
                || (isset($member->dateLeft) && $existingDateLeft->eq($member->dateLeft))) {
                $membership->dateLeft = $newLeftDate;
                $this->saveMembership($membership);
            }
        }
    }

    private function getMembershipHistoryForMemberId($memberId)
    {
        return $this->ensembleMembershipTable->find()
            ->where([
                'memberId' => $memberId
            ]);
    }
}
