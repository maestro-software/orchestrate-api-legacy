<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;

class UtilityServiceComponent extends Component {
    protected $_defaultConfig = [];

    public function convertMapToIdUpdateStructure($entityMap, $fieldNameChanges) {
        if (!isset($fieldNameChanges) || !is_array($fieldNameChanges)) {
            return $entityMap;
        }

        foreach ($fieldNameChanges as $current => $target) {
            if (isset($entityMap[$current])) {
                $entityMap[$target] = [
                    '_ids' => $entityMap[$current]
                ];

                unset($entityMap[$current]);
            }
        }

        return $entityMap;
    }

    public function array_all($array, callable $fn) {
        foreach ($array as $value) {
            if (!$fn($value)) {
                return false;
            }
        }

        return true;
    }
}
