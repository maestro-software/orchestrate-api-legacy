<?php

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\ORM\TableRegistry;
use Cake\Http\Exception\BadRequestException;
use Cake\Http\Exception\InternalErrorException;
use Cake\Datasource\Exception\RecordNotFoundException;
use Cake\i18n\Time;

class AssetServiceComponent extends Component
{
    protected $_defaultConfig = [];

    private $assetTable;

    public function initialize(array $config)
    {
        parent::initialize($config);

        /* Get the Table DAO/Table Object */
        $this->assetTable = TableRegistry::get('Asset');
    }

    public function findAll()
    {
        return $this->assetTable->find()
            ->contain($this->containAssetLoanQuery());
    }

    private function containAssetLoanQuery()
    {
        return [
            'AssetLoan.Member' => function ($q) {
                return $q->select(['id', 'firstName', 'lastName']);
            }, 'AssetLoan' => function ($q) {
                return $q->where(['dateReturned IS NULL']);
            }
        ];
    }

    public function findById($id = null)
    {
        return $this->assetTable->get($id, [
            'contain' => $this->containAssetLoanQuery()
        ]);
    }

    public function create($assetMap = null)
    {
        // Attempt to map the map to an asset
        $asset = $this->assetTable->newEntity($assetMap);
        $this->saveAsset($asset);

        return $asset;
    }

    private function saveAsset($asset)
    {
        if ($asset->getErrors()) {
            throw new BadRequestException($asset->getErrors());
        }

        if (!$this->assetTable->save($asset)) {
            throw new InternalErrorException("Invalid Asset.");
        }
    }

    public function update($id = null, $assetMap = null)
    {
        // Attempt to map the map to an asset
        $asset = $this->findById($id);
        $asset = $this->assetTable->patchEntity($asset, $assetMap);

        $this->saveAsset($asset);

        return $asset;
    }

    public function delete($id = null)
    {
        $asset = $this->findById($id);

        if (!$this->assetTable->delete($asset)) {
            throw new InternalErrorException('Couldn\'t delete asset.');
        }
    }

    public function count()
    {
        return $this->assetTable->find()->count();
    }
}
