<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 *   The code in this class can be run in the following manner:
 *   At a terminal open at the root of the project execute the following:
 *   `bin/cake createUser [<username>] [<password>]`
 */
class CreateUserShell extends Shell {
    public function main($username = 'admin', $password = 'admin') {
        $this->createRoles();
        $this->createUser($username, $password);
    }

    private function createRoles() {
        $roleTable = TableRegistry::get('Role');
        $roles = [[
            'id' => 1,
            'role' => 'admin'
        ],[
            'id' => 2,
            'role' => 'editor'
        ],[
            'id' => 3,
            'role' => 'viewer'
        ]];

        foreach ($roles as $role) {
            try {
                $roleTable->get($role['id']);
            } catch (RecordNotFoundException $e) {
                $roleObj = $roleTable->newEntity($role);
                $roleObj->id = $role['id'];

                if ($roleObj->errors()) {
                    $this->out("Invalid Role");
                    exit;
                }

                if (!$roleTable->save($roleObj)) {
                    $this->out("Failed to save role " . $role['role']);
                    exit;
                }
            }
        }
    }

    private function createUser($username, $password) {
        $usersTable = TableRegistry::get('Users');
        $newUser = [
            'username' => $username,
            'password' => $password,
            'roles' => [
                '_ids' => [1]
            ]
        ];

        $user = $usersTable->newEntity($newUser);

        if ($user->errors()) {
            $this->out("Invalid user");
            exit;
        }

        if (!$usersTable->save($user)) {
            $this->out("Failed to create user\n");
            exit;
        }

        $this->out("You can now login with " . $username . "/" . $password);
    }
}