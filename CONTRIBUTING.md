# Contribute to the Band Manager Back End

Thank you for your interest in the Band Manager. This guide details how 
to contribute to this project.

## Branching Model

This project uses the [Gitflow Workflow](https://www.atlassian.com/git/tutorials/comparing-workflows/gitflow-workflow) 
branching model. However, it differs slightly that master is for 
development and stable is used for production releases.

## Contributing

For any new features you have written please create a pull request. 
If the pull request addresses an issue, please reference it.
Every pull request should have associated tests.

## Issues

For any bugs and desired features please create an issue in the issue tracker.
