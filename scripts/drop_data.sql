-- Delete all user generated data but leave log in details

SET FOREIGN_KEY_CHECKS = 0;

TRUNCATE test_band_manager.asset;
TRUNCATE test_band_manager.asset_loan;
TRUNCATE test_band_manager.concert;
TRUNCATE test_band_manager.ensemble;
TRUNCATE test_band_manager.ensemble_membership;
TRUNCATE test_band_manager.member;
TRUNCATE test_band_manager.member_instrument;
TRUNCATE test_band_manager.performance;
TRUNCATE test_band_manager.performance_member;
TRUNCATE test_band_manager.performance_score;
TRUNCATE test_band_manager.score;

SET FOREIGN_KEY_CHECKS = 1;