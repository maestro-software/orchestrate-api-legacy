#!/bin/bash

HOST="$BACKEND_FTP_SERVER"
USERNAME="$BACKEND_FTP_USERNAME"
PASSWORD="$BACKEND_FTP_PASSWORD"

mkdir -p deploy/
tar xf dist/band-manager-backend.tar.gz -C deploy/

lftp -u "$USERNAME","$PASSWORD" $HOST << EOF
    set ftp:ssl-allow no

    !echo "FTP Upload started at $(date)"
    mkdir -p current/
    mirror -R --delete deploy/ current

    # Hacky workaround to copy the file on the server :S
    get app.php
    put app.php -o current/config/
    !rm app.php

    !echo "FTP Upload completed at $(date)"

    bye
EOF
