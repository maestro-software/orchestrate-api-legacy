<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Routing\Router;

/**
 * Add the REST endpoints, defaulting to JSON
 */
Router::scope('/', function ($routes) {
    $routes->setExtensions(['json']);

    /*
     * Explicitely add the login and logout endpoints and their associated
     * methods to call.
     *
     * Must do this as the functions are not in the Users controller.
     * These controller methods have also been added to the AppController where
     * the AuthComponent is enabled so that Auth is aware of them too.
     */
    $routes->connect('/security/login', [
        'controller' => 'Security',
        'action' => 'login',
    ]);
    $routes->connect('/security/logout', [
        'controller' => 'Security',
        'action' => 'logout',
    ]);
    $routes->connect('/security/user', [
        'controller' => 'Security',
        'action' => 'getCurrentUser',
        '_method' => 'GET',
    ]);
    $routes->connect('/security/csrf', [
        'controller' => 'Security',
        'action' => 'getCsrfToken',
        '_method' => 'GET',
    ]);
    $routes->connect('/security/request_password_reset', [
        'controller' => 'Security',
        'action' => 'requestPasswordReset',
        '_method' => 'POST',
    ]);
    $routes->connect('/security/reset_password', [
        'controller' => 'Security',
        'action' => 'resetPassword',
        '_method' => 'POST',
    ]);

    $routes->resources('Role', [
        'only' => ['index'],
    ]);

    $routes->resources('Users', function ($routes) {
        $routes->connect('/role', [
            'controller' => 'Role',
            'action' => 'byUserId',
            '_method' => 'GET',
        ]);
        $routes->connect('/role/:role_id', [
            'controller' => 'Role',
            'action' => 'addUser',
            '_method' => 'POST',
        ], ['role_id' => '[0-9]+']);
        $routes->connect('/role/:role_id', [
            'controller' => 'Role',
            'action' => 'removeUser',
            '_method' => 'DELETE',
        ], ['role_id' => '[0-9]+']);
    });

    $routes->resources('Score');

    $routes->resources('AssetLoan', [
        'only' => ['view', 'update', 'delete'],
    ]);

    $routes->resources('Asset', function ($routes) {
        $routes->connect('/loan', [
            'controller' => 'AssetLoan',
            'action' => 'byAssetId',
            '_method' => 'GET',
        ]);
        $routes->connect('/loan/current', [
            'controller' => 'AssetLoan',
            'action' => 'current',
            '_method' => 'GET',
        ]);
        $routes->connect('/loan/to/member/:member_id', [
            'controller' => 'AssetLoan',
            'action' => 'loanToMember',
            '_method' => 'POST',
        ], ['member_id' => '[0-9]+']);
        $routes->connect('/loan/return', [
            'controller' => 'AssetLoan',
            'action' => 'returnAsset',
            '_method' => 'POST',
        ]);
    });

    $routes->resources('Member', function ($routes) {
        $routes->resources('MemberInstrument', [
            'only' => ['index', 'create'],
        ]);
        $routes->connect('/member_instrument/delete', [
            'controller' => 'MemberInstrument',
            'action' => 'delete',
            '_method' => 'POST',
        ]);
        $routes->connect('/ensemble_membership', [
            'controller' => 'EnsembleMembership',
            'action' => 'membershipHistoryForMember',
            '_method' => 'GET',
        ]);
        $routes->connect('/concert', [
            'controller' => 'Concert',
            'action' => 'byMemberId',
            '_method' => 'GET',
        ]);
        $routes->connect('/invite', [
            'controller' => 'Member',
            'action' => 'invite',
            '_method' => 'POST',
        ]);
    });
    $routes->connect('/member/summary', [
        'controller' => 'MemberSummary',
        'action' => 'allMembers',
        '_method' => 'GET',
    ]);
    $routes->connect('/member/current', [
        'controller' => 'Member',
        'action' => 'currentMembers',
        '_method' => 'GET',
    ]);
    $routes->connect('/member/current/summary', [
        'controller' => 'MemberSummary',
        'action' => 'currentMembers',
        '_method' => 'GET',
    ]);

    $routes->resources('Ensemble', function ($routes) {
        $routes->resources('EnsembleMembership', [
            'only' => ['index'],
        ]);
        $routes->connect('/ensemble_membership/current', [
            'controller' => 'EnsembleMembership',
            'action' => 'currentMembersInEnsemble',
            '_method' => 'GET',
        ]);
        $routes->connect('/ensemble_membership/past', [
            'controller' => 'EnsembleMembership',
            'action' => 'pastMembersInEnsemble',
            '_method' => 'GET',
        ]);
        $routes->connect('/ensemble_membership/:member_id/join', [
            'controller' => 'EnsembleMembership',
            'action' => 'addMemberToEnsemble',
            '_method' => 'POST',
        ], ['member_id' => '[0-9]+']);
        $routes->connect('/ensemble_membership/:member_id/leave', [
            'controller' => 'EnsembleMembership',
            'action' => 'removeMemberFromEnsemble',
            '_method' => 'POST',
        ], ['member_id' => '[0-9]+']);
        $routes->connect('/role', [
            'controller' => 'Role',
            'action' => 'byEnsembleId',
            '_method' => 'GET',
        ]);
        $routes->connect('/role/:role_id', [
            'controller' => 'Role',
            'action' => 'addEnsemble',
            '_method' => 'POST',
        ], ['role_id' => '[0-9]+']);
        $routes->connect('/role/:role_id', [
            'controller' => 'Role',
            'action' => 'removeEnsemble',
            '_method' => 'DELETE',
        ], ['role_id' => '[0-9]+']);
    });
    $routes->resources('EnsembleMembership', [
        'only' => ['view', 'update', 'delete'],
    ]);

    $routes->resources('Concert', function ($routes) {
        $routes->resources('Performance', [
            'only' => ['index'],
        ]);
    });

    $routes->resources('Performance', function ($routes) {
        $routes->connect('/member', [
            'controller' => 'PerformanceMember',
            'action' => 'index',
            '_method' => 'GET',
        ]);
        $routes->connect('/member/:member_id', [
            'controller' => 'PerformanceMember',
            'action' => 'add',
            '_method' => 'POST',
        ], ['member_id' => '[0-9]+']);
        $routes->connect('/member/:member_id', [
            'controller' => 'PerformanceMember',
            'action' => 'delete',
            '_method' => 'DELETE',
        ], ['member_id' => '[0-9]+']);

        $routes->connect('/score', [
            'controller' => 'PerformanceScore',
            'action' => 'index',
            '_method' => 'GET',
        ]);
        $routes->connect('/score/:score_id', [
            'controller' => 'PerformanceScore',
            'action' => 'add',
            '_method' => 'POST',
        ], ['score_id' => '[0-9]+']);
        $routes->connect('/score/:score_id', [
            'controller' => 'PerformanceScore',
            'action' => 'delete',
            '_method' => 'DELETE',
        ], ['score_id' => '[0-9]+']);
    });
});
