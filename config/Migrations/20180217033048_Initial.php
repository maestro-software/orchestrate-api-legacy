<?php
use Migrations\AbstractMigration;

class Initial extends AbstractMigration
{
    public function up()
    {

        $this->table('asset')
            ->addColumn('description', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => false,
            ])
            ->addColumn('quantity', 'integer', [
                'default' => '1',
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('serialNo', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('dateAquired', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => false,
            ])
            ->addColumn('valuePaid', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('dateDiscarded', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('valueDiscarded', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('insuranceValue', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('currentLocation', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('notes', 'string', [
                'default' => null,
                'limit' => 2000,
                'null' => true,
            ])
            ->create();

        $this->table('asset_loan')
            ->addColumn('memberId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('assetId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('dateBorrowed', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('dateReturned', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'assetId',
                ]
            )
            ->addIndex(
                [
                    'memberId',
                ]
            )
            ->create();

        $this->table('concert')
            ->addColumn('occasion', 'string', [
                'default' => null,
                'limit' => 90,
                'null' => false,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 90,
                'null' => true,
            ])
            ->addColumn('date', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('notes', 'string', [
                'default' => null,
                'limit' => 2000,
                'null' => true,
            ])
            ->create();

        $this->table('ensemble')
            ->addColumn('name', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('isHidden', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'name',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('ensemble_membership')
            ->addColumn('memberId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('ensembleId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('dateJoined', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('dateLeft', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'ensembleId',
                ]
            )
            ->addIndex(
                [
                    'memberId',
                ]
            )
            ->create();

        $this->table('member')
            ->addColumn('firstName', 'string', [
                'default' => null,
                'limit' => 35,
                'null' => false,
            ])
            ->addColumn('lastName', 'string', [
                'default' => null,
                'limit' => 35,
                'null' => false,
            ])
            ->addColumn('phoneNo', 'string', [
                'default' => null,
                'limit' => 12,
                'null' => true,
            ])
            ->addColumn('email', 'string', [
                'default' => null,
                'limit' => 90,
                'null' => false,
            ])
            ->addColumn('address', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('feeClass', 'string', [
                'default' => 'Standard',
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('membershipClass', 'string', [
                'default' => 'Ordinary',
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('dateJoined', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('dateLeft', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('notes', 'string', [
                'default' => null,
                'limit' => 1000,
                'null' => true,
            ])
            ->create();

        $this->table('member_instrument')
            ->addColumn('memberId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('instrumentName', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addIndex(
                [
                    'memberId',
                ]
            )
            ->create();

        $this->table('performance')
            ->addColumn('ensembleId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('concertId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('countsAsSeperate', 'boolean', [
                'default' => false,
                'limit' => null,
                'null' => false,
            ])
            ->addIndex(
                [
                    'concertId',
                ]
            )
            ->addIndex(
                [
                    'ensembleId',
                ]
            )
            ->create();

        $this->table('performance_member')
            ->addColumn('memberId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('performanceId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'memberId',
                    'performanceId',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'memberId',
                ]
            )
            ->addIndex(
                [
                    'performanceId',
                ]
            )
            ->create();

        $this->table('performance_score')
            ->addColumn('performanceId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('scoreId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'performanceId',
                    'scoreId',
                ],
                ['unique' => true]
            )
            ->addIndex(
                [
                    'performanceId',
                ]
            )
            ->addIndex(
                [
                    'scoreId',
                ]
            )
            ->create();

        $this->table('role')
            ->addColumn('role', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addIndex(
                [
                    'role',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('score')
            ->addColumn('title', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => false,
            ])
            ->addColumn('composer', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('arranger', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('genre', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('grade', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 2,
                'scale' => 1,
            ])
            ->addColumn('duration', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => true,
            ])
            ->addColumn('datePurchased', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('valuePaid', 'decimal', [
                'default' => null,
                'null' => true,
                'precision' => 10,
                'scale' => 2,
            ])
            ->addColumn('location', 'string', [
                'default' => null,
                'limit' => 45,
                'null' => true,
            ])
            ->addColumn('notes', 'string', [
                'default' => null,
                'limit' => 2000,
                'null' => true,
            ])
            ->create();

        $this->table('user_role')
            ->addColumn('userId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addColumn('roleId', 'integer', [
                'default' => null,
                'limit' => 11,
                'null' => false,
            ])
            ->addIndex(
                [
                    'roleId',
                ]
            )
            ->addIndex(
                [
                    'userId',
                ]
            )
            ->create();

        $this->table('users')
            ->addColumn('username', 'string', [
                'default' => null,
                'limit' => 50,
                'null' => false,
            ])
            ->addColumn('password', 'string', [
                'default' => null,
                'limit' => 255,
                'null' => false,
            ])
            ->addColumn('role', 'string', [
                'default' => null,
                'limit' => 20,
                'null' => false,
            ])
            ->addColumn('created', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addColumn('modified', 'datetime', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addIndex(
                [
                    'username',
                ],
                ['unique' => true]
            )
            ->create();

        $this->table('asset_loan')
            ->addForeignKey(
                'assetId',
                'asset',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'memberId',
                'member',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('ensemble_membership')
            ->addForeignKey(
                'ensembleId',
                'ensemble',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->addForeignKey(
                'memberId',
                'member',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('member_instrument')
            ->addForeignKey(
                'memberId',
                'member',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('performance')
            ->addForeignKey(
                'concertId',
                'concert',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'ensembleId',
                'ensemble',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'NO_ACTION'
                ]
            )
            ->update();

        $this->table('performance_member')
            ->addForeignKey(
                'memberId',
                'member',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'performanceId',
                'performance',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('performance_score')
            ->addForeignKey(
                'performanceId',
                'performance',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'scoreId',
                'score',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();

        $this->table('user_role')
            ->addForeignKey(
                'roleId',
                'role',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'CASCADE'
                ]
            )
            ->addForeignKey(
                'userId',
                'users',
                'id',
                [
                    'update' => 'NO_ACTION',
                    'delete' => 'CASCADE'
                ]
            )
            ->update();
    }

    public function down()
    {
        $this->table('asset_loan')
            ->dropForeignKey(
                'assetId'
            )
            ->dropForeignKey(
                'memberId'
            );

        $this->table('ensemble_membership')
            ->dropForeignKey(
                'ensembleId'
            )
            ->dropForeignKey(
                'memberId'
            );

        $this->table('member_instrument')
            ->dropForeignKey(
                'memberId'
            );

        $this->table('performance')
            ->dropForeignKey(
                'concertId'
            )
            ->dropForeignKey(
                'ensembleId'
            );

        $this->table('performance_member')
            ->dropForeignKey(
                'memberId'
            )
            ->dropForeignKey(
                'performanceId'
            );

        $this->table('performance_score')
            ->dropForeignKey(
                'performanceId'
            )
            ->dropForeignKey(
                'scoreId'
            );

        $this->table('user_role')
            ->dropForeignKey(
                'roleId'
            )
            ->dropForeignKey(
                'userId'
            );

        $this->dropTable('asset');
        $this->dropTable('asset_loan');
        $this->dropTable('concert');
        $this->dropTable('ensemble');
        $this->dropTable('ensemble_membership');
        $this->dropTable('member');
        $this->dropTable('member_instrument');
        $this->dropTable('performance');
        $this->dropTable('performance_member');
        $this->dropTable('performance_score');
        $this->dropTable('role');
        $this->dropTable('score');
        $this->dropTable('user_role');
        $this->dropTable('users');
    }
}
