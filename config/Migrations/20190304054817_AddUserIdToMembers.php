<?php
use Migrations\AbstractMigration;

class AddUserIdToMembers extends AbstractMigration
{
    /**
     * Change Method.
     *
     * More information on this method is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-change-method
     * @return void
     */
    public function change()
    {
        $table = $this->table('member')
            ->addColumn('userId', 'integer', [
                'default' => null,
                'limit' => null,
                'null' => true,
            ])
            ->addForeignKey(
                'userId',
                'users',
                'id',
                [
                    'update' => 'CASCADE',
                    'delete' => 'SET NULL',
                ]
            )
            ->update();
    }
}
